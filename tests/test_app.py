# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import zlib

from flask import current_app
from flask import json
from itsdangerous import base64_decode

from tests.utils import get_cookie


def _decode_session_cookie(cookie):
    compressed = False

    if cookie.startswith("."):
        compressed = True
        cookie = cookie[1:]

    data = cookie.split(".")[0]
    data = base64_decode(data)

    if compressed:
        data = zlib.decompress(data)

    return json.loads(data.decode())


def test_session_cookie(client, user_session):
    """Test if the session cookie is set correctly."""
    session_cookie_name = current_app.config["SESSION_COOKIE_NAME"]

    with user_session():
        # Set the session cookie.
        client.get("/")

        session_cookie = get_cookie(client, session_cookie_name)
        session_cookie_dict = _decode_session_cookie(session_cookie.value)

        assert len(session_cookie_dict) == 4

        # Flask-Login values and the CSRF token from Flask-WTF.
        for key in ["_id", "_fresh", "_user_id", "csrf_token"]:
            assert key in session_cookie_dict

    session_cookie = get_cookie(client, session_cookie_name)
    session_cookie_dict = _decode_session_cookie(session_cookie.value)

    assert len(session_cookie_dict) == 1
    # Only the CSRF token should remain.
    assert "csrf_token" in session_cookie_dict

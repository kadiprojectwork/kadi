# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
from io import BytesIO

import pytest

from kadi.lib.exceptions import KadiChecksumMismatchError
from kadi.lib.exceptions import KadiFilesizeExceededError
from kadi.lib.exceptions import KadiFilesizeMismatchError
from kadi.lib.security import hash_value
from kadi.lib.storage.local import LocalStorage


def test_local_storage_filepath_from_name():
    """Test if creating a filepath from a name works correctly in the local storage."""
    assert LocalStorage.filepath_from_name("abcdef", dir_len=2, num_dirs=3) is None
    assert LocalStorage.filepath_from_name(
        "abcdefg", dir_len=2, num_dirs=3
    ) == os.path.join("ab", "cd", "ef", "g")


def test_local_storage_make_absolute(tmp_path):
    """Test if creating an absolute directory works correctly in the local storage."""
    storage = LocalStorage(root_directory=tmp_path)

    # Try creating an absolute path from a relative path.
    assert storage._make_absolute("test.txt") == os.path.join(
        storage.root_directory, "test.txt"
    )

    # Try creating an absolute path that is the root directory.
    assert storage._make_absolute("") == storage.root_directory

    # Try creating a path outside of the root directory with an absolute path.
    with pytest.raises(ValueError):
        storage._make_absolute(os.path.join(storage.root_directory, "..", "test.txt"))

    # Try creating a path outside of the root directory with a relative path.
    with pytest.raises(ValueError):
        storage._make_absolute(os.path.join("..", "..", "test.txt"))


def test_local_storage(tmp_path):
    """Test if the local storage itself works correctly."""

    # Try creating a storage with non absolute root directory.
    with pytest.raises(ValueError):
        LocalStorage(root_directory="test")

    file_data = 10 * b"x"
    storage = LocalStorage(root_directory=tmp_path, max_size=len(file_data))

    first_filepath = os.path.join(storage.root_directory, "test", "first.txt")
    second_filepath = os.path.join(storage.root_directory, "test", "second.txt")

    # Try saving too much data.
    with pytest.raises(KadiFilesizeExceededError):
        storage.save(first_filepath, BytesIO(file_data + b"x"))

    # Try saving, copying and appending some data to both files.
    storage.save(first_filepath, BytesIO(file_data[: len(file_data) // 2]))
    storage.save(second_filepath, first_filepath)
    storage.save(second_filepath, first_filepath, append=True)

    assert os.path.isfile(first_filepath)
    assert os.path.isfile(second_filepath)

    # Try checking if both files exist according to the storage as well.
    storage.exists(first_filepath)
    storage.exists(second_filepath)

    # Try moving the second file in the place of the first file.
    storage.move(second_filepath, first_filepath)

    assert os.path.isfile(first_filepath)
    assert not os.path.isfile(second_filepath)

    # Try opening and reading from the first file.
    file = storage.open(first_filepath)
    assert file.read() == file_data

    # Try closing the first file after opening it.
    storage.close(file)
    assert file.closed

    # Try determining the MIME type of the first file.
    assert storage.get_mimetype(first_filepath) == "text/plain"

    # Try determining and validating the size of the first file.
    assert storage.get_size(first_filepath) == len(file_data)

    storage.validate_size(first_filepath, len(file_data), "==")
    with pytest.raises(KadiFilesizeMismatchError):
        storage.validate_size(first_filepath, len(file_data), "!=")

    # Try determining and validating the checksum of the first file.
    checksum = hash_value(file_data, alg="md5")
    assert storage.get_checksum(first_filepath) == checksum

    storage.validate_checksum(first_filepath, checksum)
    with pytest.raises(KadiChecksumMismatchError):
        storage.validate_checksum(first_filepath, "test")

    # Restore the second file, then try deleting the files one by one, checking if empty
    # parent directories are deleted accordingly.
    storage.move(first_filepath, second_filepath)
    storage.delete(first_filepath)

    assert not os.path.isfile(first_filepath)
    assert os.path.isfile(second_filepath)
    assert os.listdir(storage.root_directory)

    storage.delete(second_filepath)

    assert not os.path.isfile(second_filepath)
    assert not os.listdir(storage.root_directory)
    assert os.path.isdir(storage.root_directory)

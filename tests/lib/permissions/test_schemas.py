# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.permissions.core import add_role
from kadi.lib.permissions.schemas import GroupRoleSchema
from kadi.lib.permissions.schemas import UserRoleSchema
from kadi.lib.permissions.utils import get_group_roles
from kadi.lib.permissions.utils import get_user_roles


def test_user_role_schema_dump_from_iterable(dummy_record):
    """Test if dumping user roles with the "UserRoleSchema" works correctly."""
    user_roles = UserRoleSchema(obj=dummy_record).dump_from_iterable(
        get_user_roles("record")
    )

    user_role = user_roles[0]
    assert "user" in user_role
    assert "role" in user_role
    assert "_actions" in user_role and user_role["_actions"]


def test_group_role_schema_dump_from_iterable(dummy_group, dummy_record):
    """Test if dumping group roles with the "GroupRoleSchema" works correctly."""
    add_role(dummy_group, "record", dummy_record.id, "member")
    group_roles = GroupRoleSchema(obj=dummy_record).dump_from_iterable(
        get_group_roles("record")
    )

    group_role = group_roles[0]
    assert "group" in group_role
    assert "role" in group_role
    assert "_actions" in group_role and group_role["_actions"]

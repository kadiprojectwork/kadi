# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from uuid import uuid4

import pytest

from kadi.lib.exceptions import KadiValidationError
from kadi.lib.validation import validate_identifier
from kadi.lib.validation import validate_mimetype
from kadi.lib.validation import validate_username
from kadi.lib.validation import validate_uuid


def _validate_value(validator, value, is_valid):
    if is_valid:
        validator(value)
    else:
        with pytest.raises(KadiValidationError):
            validator(value)


@pytest.mark.parametrize("value,is_valid", [(str(uuid4()), True), ("test", False)])
def test_validate_uuid(value, is_valid):
    """Test if UUIDs are validated correctly."""
    _validate_value(validate_uuid, value, is_valid)


@pytest.mark.parametrize(
    "value,is_valid",
    [
        ("-", True),
        ("_", True),
        ("a", True),
        ("0", True),
        ("0-9_a-_z", True),
        ("", False),
        (" ", False),
        (" a", False),
        ("A", False),
        ("!", False),
    ],
)
def test_validate_identifier(value, is_valid):
    """Test if identifiers are validated correctly."""
    _validate_value(validate_identifier, value, is_valid)


@pytest.mark.parametrize(
    "value,is_valid",
    [
        ("a/bc", True),
        ("a/b-z+0.9", True),
        ("", False),
        (" ", False),
        ("/a", False),
        ("0/a", False),
        ("a/A", False),
        ("a/!", False),
    ],
)
def test_validate_mimetype(value, is_valid):
    """Test if MIME types are validated correctly."""
    _validate_value(validate_mimetype, value, is_valid)


@pytest.mark.parametrize(
    "value,is_valid",
    [
        ("a", True),
        ("0", True),
        ("a-z", True),
        ("0_9", True),
        ("a-z_0-9", True),
        ("", False),
        (" ", False),
        (" a", False),
        ("A", False),
        ("!", False),
        ("-", False),
        ("_", False),
        ("a-", False),
        ("-a", False),
        ("a_", False),
        ("_a", False),
        ("a--0", False),
        ("a__0", False),
        ("a-_0", False),
        ("a_-0", False),
    ],
)
def test_validate_username(value, is_valid):
    """Test if local usernames are validated correctly."""
    _validate_value(validate_username, value, is_valid)

# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app
from werkzeug.exceptions import default_exceptions
from werkzeug.http import HTTP_STATUS_CODES

from kadi.lib.api.core import check_access_token_scopes
from kadi.lib.api.core import create_access_token
from kadi.lib.api.core import json_error_response
from kadi.lib.api.core import json_response
from kadi.lib.api.models import AccessToken
from kadi.lib.api.models import AccessTokenScope
from kadi.lib.utils import utcnow
from kadi.lib.web import url_for
from tests.utils import check_api_response


def test_create_access_token(db, dummy_user):
    """Test if access tokens are created correctly."""
    token = AccessToken.new_token()
    access_token = create_access_token(
        name="test",
        user=dummy_user,
        expires_at=utcnow(),
        token=token,
        scopes=["record.read", "collection.read"],
    )
    db.session.commit()

    assert AccessToken.query.filter_by(name="test").one() == access_token
    assert AccessToken.get_by_token(token) == access_token
    assert access_token.is_expired
    assert access_token.scopes.count() == 2
    assert (
        access_token.scopes.filter(AccessTokenScope.object == "record").one().action
        == "read"
    )


def test_json_response():
    """Test if JSON responses work correctly."""
    response = json_response(200, {"test": "test"}, headers={"test": "test"})

    check_api_response(response)
    assert "test" in response.get_json()
    assert "test" in response.headers


def test_json_error_response():
    """Test if JSON error responses work correctly."""
    response = json_error_response(
        404,
        message="message",
        description="description",
        test="test",
        headers={"test": "test"},
    )
    data = response.get_json()

    check_api_response(response, status_code=404)
    assert response.headers["test"] == "test"
    assert data["code"] == 404
    assert data["message"] == "message"
    assert data["description"] == "description"
    assert data["test"] == "test"

    # Check default information.
    response = json_error_response(404)
    data = response.get_json()

    check_api_response(response, status_code=404)
    assert data["code"] == 404
    assert data["message"] == HTTP_STATUS_CODES.get(404)
    assert data["description"] == default_exceptions.get(404).description


def test_check_access_token_scopes(new_access_token):
    """Test if scopes of personal access tokens are checked correctly."""
    full_access_token = new_access_token()
    scoped_token = new_access_token(scopes=["record.read"])

    # Test without a request context.
    assert check_access_token_scopes("test.test")

    # Test a token with full access.
    with current_app.test_request_context(
        headers={"Authorization": f"Bearer {full_access_token}"}
    ):
        assert check_access_token_scopes("test.test")

    # Test a scoped token.
    with current_app.test_request_context(
        headers={"Authorization": f"Bearer {scoped_token}"}
    ):
        assert check_access_token_scopes("record.read")
        assert check_access_token_scopes(
            "record.read", "collection.read", operator="or"
        )
        assert not check_access_token_scopes("test.test")
        assert not check_access_token_scopes("collection.read")
        assert not check_access_token_scopes("record.read", "collection.read")


def test_scopes_required(
    api_client, client, dummy_record, new_access_token, user_session
):
    """Test if the "scopes_required" decorator works correctly."""
    endpoint = url_for("api.get_record", id=dummy_record.id)

    # Test the session without any token.
    with user_session():
        response = client.get(endpoint)
        check_api_response(response)

    # Test a token with full access.
    token = new_access_token()
    response = api_client(token).get(endpoint)
    check_api_response(response)

    # Test a scoped token without access.
    token = new_access_token(scopes=["collection.read"])
    response = api_client(token).get(endpoint)
    data = response.get_json()

    check_api_response(response, status_code=401)
    assert data["description"] == "Access token has insufficient scope."
    assert data["scopes"] == ["record.read"]

    # Test a scoped token with access.
    token = new_access_token(scopes=["record.read"])
    response = api_client(token).get(endpoint)
    check_api_response(response)


def test_internal(api_client, client, dummy_access_token, user_session):
    """Test if the "internal" decorator works correctly."""
    endpoint = url_for("api.select_tags")

    with user_session():
        response = client.get(endpoint)
        check_api_response(response)

    response = api_client(dummy_access_token).get(endpoint)
    check_api_response(response, status_code=404)

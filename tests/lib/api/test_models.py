# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import timedelta

from kadi.lib.api.models import AccessToken
from kadi.lib.utils import utcnow


def test_is_expired(dummy_user):
    """Test if the expiration date of access tokens works correctly."""
    access_token = AccessToken.create(user=dummy_user, name="test")
    assert not access_token.is_expired

    access_token = AccessToken.create(
        user=dummy_user, name="test", expires_at=utcnow() + timedelta(seconds=10)
    )
    assert not access_token.is_expired

    access_token = AccessToken.create(
        user=dummy_user, name="test", expires_at=utcnow() - timedelta(seconds=10)
    )
    assert access_token.is_expired


def test_get_by_token(dummy_user):
    """Test if retrieving a token works correctly."""
    token = AccessToken.new_token()
    access_token = AccessToken.create(user=dummy_user, name="test", token=token)

    assert AccessToken.get_by_token(token) == access_token


def test_create(dummy_user):
    """Test if creating new access tokens works correctly."""
    token = AccessToken.new_token()
    access_token = AccessToken.create(user=dummy_user, name="test", token=token)

    assert AccessToken.hash_token(token) == access_token.token_hash

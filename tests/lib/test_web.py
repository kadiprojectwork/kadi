# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from urllib.parse import urlparse

from flask import current_app

import kadi.lib.constants as const
from kadi.lib.conversion import lower
from kadi.lib.conversion import normalize
from kadi.lib.web import download_bytes
from kadi.lib.web import download_stream
from kadi.lib.web import get_locale
from kadi.lib.web import get_next_url
from kadi.lib.web import make_next_url
from kadi.lib.web import paginated
from kadi.lib.web import qparam
from kadi.lib.web import static_url
from kadi.lib.web import url_for


def test_get_locale(client):
    """Test if determining the locale works correctly."""
    new_locale = "de"
    default_locale = const.LOCALE_DEFAULT

    with client:
        # Outside a request context.
        assert get_locale() == default_locale

        client.get(url_for("main.index"))
        assert get_locale() == default_locale

        # Using the request argument.
        client.get(url_for("main.index", locale=new_locale))
        assert get_locale() == new_locale

        # Using the cookie.
        client.get(url_for("main.index"))
        assert get_locale() == new_locale

        # Using the default locale as fallback for an invalid value.
        client.get(url_for("main.index", locale="invalid"))
        assert get_locale() == default_locale


def test_download_bytes(request_context):
    """Test if downloading binary data works correctly."""
    data = b"test"
    filename = "test.txt"
    response = download_bytes(data, filename)
    response.direct_passthrough = False

    assert response.status_code == 200
    assert response.data == data
    assert response.content_length == len(data)
    assert response.mimetype == "text/plain"
    assert response.headers["Content-Disposition"] == f"attachment; filename={filename}"


def test_download_stream(request_context):
    """Test if downloading streamed data works correctly."""
    stream = [b"foo", b" ", b"bar"]
    data = b"".join(stream)
    filename = "test.txt"
    response = download_stream(stream, filename, content_length=len(data))
    response.direct_passthrough = False

    assert response.status_code == 200
    assert response.data == data
    assert response.content_length == len(data)
    assert response.mimetype == "text/plain"
    assert response.headers["Content-Disposition"] == f"attachment; filename={filename}"


def test_paginated():
    """Test if the "paginated" decorator works correctly."""

    # pylint: disable=no-value-for-parameter
    @paginated
    def _paginated_func(page, per_page):
        assert page == 1
        assert per_page == 100

    with current_app.test_request_context("?page=-1&per_page=200"):
        _paginated_func()

    @paginated(page_max=5)
    def _paginated_func(page, per_page):
        assert page == 5
        assert per_page == 10

    with current_app.test_request_context("?page=100&per_page=test"):
        _paginated_func()


def test_qparam():
    """Test if the "qparam" decorator works correctly."""

    # pylint: disable=no-value-for-parameter
    @qparam("var_1", location="param_1", multiple=True, parse=int)
    @qparam("param_2", multiple=True, default="ignored", parse=[float, int])
    @qparam("param_3", parse=[lower, normalize])
    @qparam("param_4", default=0, parse=[int, lower])
    @qparam("param_5", default=None, parse=int)
    @qparam("param_6", default=lambda: "test")
    @qparam("param_7", multiple=True)
    @qparam("param_8")
    def _parametrized_func(qparams):
        assert qparams["var_1"] == [1, 2]
        assert qparams["param_2"] == []
        assert qparams["param_3"] == "test"
        assert qparams["param_4"] == 0
        assert qparams["param_5"] is None
        assert qparams["param_6"] == "test"
        assert qparams["param_7"] == []
        assert qparams["param_8"] == ""

    with current_app.test_request_context(
        "?param_1=1&param_1=a&param_1=2&param_2=&param_3= TEST &param_4=a"
    ):
        _parametrized_func()


def test_url_for(api_client, dummy_access_token):
    """Test if URLs are generated correctly."""
    base_url = (
        f"{current_app.config['PREFERRED_URL_SCHEME']}://"
        f"{current_app.config['SERVER_NAME']}"
    )

    # Test the general URL generation without a request context.
    assert url_for("main.index") == f"{base_url}/"
    assert url_for("api.index") == f"{base_url}/api"
    assert url_for("api.index_v1") == f"{base_url}/api/v1"
    assert url_for("api.index_v1", _ignore_version=True) == f"{base_url}/api/v1"

    # Test the API versioning with a request context.
    with api_client(dummy_access_token) as _api_client:
        # Make a request to a non-versioned endpoint.
        _api_client.get(url_for("api.index"))

        assert url_for("api.index") == f"{base_url}/api"

        # Make a request to a versioned endpoint.
        _api_client.get(url_for("api.index_v1"))

        assert url_for("api.index") == f"{base_url}/api/v1"
        assert url_for("api.index", _ignore_version=True) == f"{base_url}/api"
        # Check an endpoint that is not versioned.
        assert url_for("api.select_tags") == f"{base_url}/api/tags/select"


def test_static_url(monkeypatch):
    """Test if static URLs are generated correctly."""
    base_url = (
        f"{current_app.config['PREFERRED_URL_SCHEME']}://"
        f"{current_app.config['SERVER_NAME']}"
    )

    assert static_url("test") == f"{base_url}/static/test"

    monkeypatch.setitem(current_app.config, "MANIFEST_MAPPING", {"foo": "bar"})

    assert static_url("test") == f"{base_url}/static/test"
    assert static_url("foo") == f"{base_url}/static/bar"


def test_next_url(client):
    """Test if creating and validating the "next" URL works correctly."""
    fallback_url = url_for("main.index")
    valid_url = url_for("records.records")
    invalid_url = "http://example.com"

    assert make_next_url(valid_url) == url_for(
        "accounts.login", next=urlparse(valid_url).path
    )
    assert make_next_url(invalid_url) == url_for("accounts.login", next=invalid_url)

    with client:
        # Outside a request context.
        assert get_next_url() == fallback_url

        # Using an absolute URL.
        client.get(url_for("main.index", next=valid_url))
        assert get_next_url() == valid_url

        # Using a relative URL.
        client.get(url_for("main.index", next=urlparse(valid_url).path))
        assert get_next_url() == urlparse(valid_url).path

        # Using an invalid URL.
        client.get(url_for("main.index", next=invalid_url))
        assert get_next_url() == fallback_url

# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

from kadi.lib.permissions.core import add_role
from kadi.lib.storage.misc import create_misc_storage
from kadi.modules.groups.utils import delete_group_image
from kadi.modules.groups.utils import get_user_groups
from kadi.modules.groups.utils import save_group_image


def test_get_user_groups(dummy_group, dummy_user, new_user):
    """Test if determining user group membership works correctly."""
    user = new_user()

    assert get_user_groups(dummy_user).one() == dummy_group
    assert not get_user_groups(user).all()

    add_role(user, "group", dummy_group.id, "member")

    assert get_user_groups(user).one() == dummy_group


def test_save_group_image(dummy_image, dummy_group):
    """Test if saving a group image works correctly."""
    storage = create_misc_storage()

    save_group_image(dummy_group, dummy_image)
    first_image_name = dummy_group.image_name

    assert first_image_name is not None
    assert storage.exists(storage.create_filepath(str(first_image_name)))

    dummy_image.seek(0)
    save_group_image(dummy_group, dummy_image)
    second_image_name = dummy_group.image_name

    assert second_image_name is not None
    assert not storage.exists(storage.create_filepath(str(first_image_name)))
    assert storage.exists(storage.create_filepath(str(second_image_name)))


def test_delete_group_image(dummy_image, dummy_group):
    """Test if deleting a group image works correctly."""
    storage = create_misc_storage()

    save_group_image(dummy_group, dummy_image)
    delete_group_image(dummy_group)

    assert dummy_group.image_name is None
    assert not os.listdir(storage.root_directory)

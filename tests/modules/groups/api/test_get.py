# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.web import url_for
from kadi.modules.groups.utils import save_group_image
from tests.utils import check_api_response


def test_preview_group_image(client, dummy_image, dummy_group, user_session):
    """Test the internal "api.preview_group_image" endpoint."""
    save_group_image(dummy_group, dummy_image)

    with user_session():
        response = client.get(url_for("api.preview_group_image", id=dummy_group.id))
        check_api_response(response, content_type="image/jpeg")
        response.close()


def test_get_group_role_rules(client, user_session, dummy_group):
    """Test the internal "api.get_group_role_rules" endpoint."""
    with user_session():
        response = client.get(url_for("api.get_group_role_rules", id=dummy_group.id))
        check_api_response(response)


def test_select_groups(client, user_session):
    """Test the internal "api.select_groups" endpoint."""
    with user_session():
        response = client.get(url_for("api.select_groups"))
        check_api_response(response)


def test_get_groups(api_client, dummy_access_token):
    """Test the "api.get_groups" endpoint."""
    response = api_client(dummy_access_token).get(url_for("api.get_groups"))
    check_api_response(response)


def test_get_group_by_identifier(api_client, dummy_access_token, dummy_group):
    """Test the "api.get_group_by_identifier" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_group_by_identifier", identifier=dummy_group.identifier)
    )
    check_api_response(response)


def test_get_group_revision(api_client, dummy_access_token, dummy_group):
    """Test the "api.get_group_revision" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for(
            "api.get_group_revision",
            group_id=dummy_group.id,
            revision_id=dummy_group.ordered_revisions.first().id,
        )
    )
    check_api_response(response)


@pytest.mark.parametrize(
    "endpoint",
    [
        "get_group",
        "get_group_members",
        "get_group_revisions",
        "get_group_records",
        "get_group_collections",
        "get_group_templates",
    ],
)
def test_get_group_endpoints(endpoint, api_client, dummy_access_token, dummy_group):
    """Test the remaining "api.get_group*" endpoints."""
    response = api_client(dummy_access_token).get(
        url_for(f"api.{endpoint}", id=dummy_group.id)
    )
    check_api_response(response)

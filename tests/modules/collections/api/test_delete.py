# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.resources.utils import add_link
from kadi.lib.web import url_for
from kadi.modules.collections.core import link_collections
from kadi.modules.collections.models import CollectionState
from tests.modules.utils import check_api_delete_subject_resource_role
from tests.utils import check_api_response


def test_delete_collection(api_client, dummy_access_token, dummy_collection):
    """Test the "api.delete_collection" endpoint."""
    response = api_client(dummy_access_token).delete(
        url_for("api.delete_collection", id=dummy_collection.id)
    )

    check_api_response(response, status_code=204)
    assert dummy_collection.state == CollectionState.DELETED


def test_remove_collection_record(
    api_client, db, dummy_access_token, dummy_collection, dummy_record, dummy_user
):
    """Test the "api.remove_collection_record" endpoint."""
    add_link(dummy_collection.records, dummy_record, user=dummy_user)
    db.session.commit()

    response = api_client(dummy_access_token).delete(
        url_for(
            "api.remove_collection_record",
            collection_id=dummy_collection.id,
            record_id=dummy_record.id,
        )
    )
    db.session.commit()

    check_api_response(response, status_code=204)
    assert not dummy_collection.records.all()


def test_remove_child_collection(
    api_client, db, dummy_access_token, dummy_collection, dummy_user, new_collection
):
    """Test the "api.remove_child_collection" endpoint."""
    collection = new_collection()
    link_collections(dummy_collection, collection, user=dummy_user)
    db.session.commit()

    response = api_client(dummy_access_token).delete(
        url_for(
            "api.remove_child_collection",
            collection_id=dummy_collection.id,
            child_id=collection.id,
        )
    )
    db.session.commit()

    check_api_response(response, status_code=204)
    assert not dummy_collection.children.all()


def test_remove_collection_user_role(
    api_client, db, dummy_access_token, dummy_collection, dummy_user, new_user
):
    """Test the "api.remove_collection_user_role" endpoint."""
    user = new_user()
    client = api_client(dummy_access_token)
    endpoint = url_for(
        "api.remove_collection_user_role",
        collection_id=dummy_collection.id,
        user_id=user.id,
    )
    remove_creator_endpoint = url_for(
        "api.remove_collection_user_role",
        collection_id=dummy_collection.id,
        user_id=dummy_user.id,
    )

    check_api_delete_subject_resource_role(
        db,
        client,
        endpoint,
        user,
        dummy_collection,
        remove_creator_endpoint=remove_creator_endpoint,
    )


def test_remove_collection_group_role(
    api_client, db, dummy_access_token, dummy_collection, dummy_group
):
    """Test the "api.remove_collection_group_role" endpoint."""
    client = api_client(dummy_access_token)
    endpoint = url_for(
        "api.remove_collection_group_role",
        collection_id=dummy_collection.id,
        group_id=dummy_group.id,
    )

    check_api_delete_subject_resource_role(
        db, client, endpoint, dummy_group, dummy_collection
    )

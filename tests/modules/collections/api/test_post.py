# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from kadi.modules.collections.core import delete_collection
from kadi.modules.collections.models import Collection
from kadi.modules.collections.models import CollectionState
from tests.modules.utils import check_api_post_subject_resource_role
from tests.utils import check_api_response


def test_new_collection(api_client, dummy_access_token):
    """Test the "api.new_collection" endpoint."""
    response = api_client(dummy_access_token).post(
        url_for("api.new_collection"), json={"identifier": "test", "title": "test"}
    )

    check_api_response(response, status_code=201)
    assert Collection.query.filter_by(identifier="test").one()


def test_add_collection_record(
    api_client, dummy_access_token, dummy_collection, dummy_record
):
    """Test the "api.add_collection_record" endpoint."""
    response = api_client(dummy_access_token).post(
        url_for("api.add_collection_record", id=dummy_collection.id),
        json={"id": dummy_record.id},
    )

    check_api_response(response, status_code=201)
    assert dummy_collection.records.one() == dummy_record


def test_add_child_collection(
    api_client, dummy_access_token, dummy_collection, new_collection
):
    """Test the "api.add_child_collection" endpoint."""
    collection = new_collection()
    response = api_client(dummy_access_token).post(
        url_for("api.add_child_collection", id=dummy_collection.id),
        json={"id": collection.id},
    )

    check_api_response(response, status_code=201)
    assert dummy_collection.children.one() == collection


def test_add_collection_user_role(
    api_client, db, dummy_access_token, dummy_collection, new_user
):
    """Test the "api.add_collection_user_role" endpoint."""
    client = api_client(dummy_access_token)
    endpoint = url_for("api.add_collection_user_role", id=dummy_collection.id)

    check_api_post_subject_resource_role(
        db, client, endpoint, new_user(), dummy_collection
    )


def test_add_collection_group_role(
    api_client, db, dummy_access_token, dummy_collection, dummy_group
):
    """Test the "api.add_collection_group_role" endpoint."""
    client = api_client(dummy_access_token)
    endpoint = url_for("api.add_collection_group_role", id=dummy_collection.id)

    check_api_post_subject_resource_role(
        db, client, endpoint, dummy_group, dummy_collection
    )


def test_restore_collection(
    api_client, dummy_access_token, dummy_collection, dummy_user
):
    """Test the "api.restore_collection" endpoint."""
    delete_collection(dummy_collection, user=dummy_user)

    response = api_client(dummy_access_token).post(
        url_for("api.restore_collection", id=dummy_collection.id)
    )

    check_api_response(response)
    assert dummy_collection.state == CollectionState.ACTIVE


def test_purge_collection(api_client, dummy_access_token, dummy_collection, dummy_user):
    """Test the "api.purge_collection" endpoint."""
    delete_collection(dummy_collection, user=dummy_user)

    response = api_client(dummy_access_token).post(
        url_for("api.purge_collection", id=dummy_collection.id)
    )

    check_api_response(response, status_code=204)
    assert Collection.query.get(dummy_collection.id) is None

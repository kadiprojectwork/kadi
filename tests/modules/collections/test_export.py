# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import functools
import os
import zipfile
from io import BytesIO

import pytest
from flask import json

from kadi.lib.resources.utils import add_link
from kadi.modules.collections.export import get_export_data


@pytest.mark.parametrize("exclude_records", [True, False])
def test_get_export_data_json(
    exclude_records, dummy_collection, dummy_record, dummy_user
):
    """Test if the collection JSON export works correctly."""
    add_link(dummy_collection.records, dummy_record, user=dummy_user)

    export_filter = {"user": True, "records": exclude_records}
    json_data = get_export_data(
        dummy_collection, "json", export_filter=export_filter, user=dummy_user
    )

    assert json_data is not None

    collection_data = json.loads(json_data.read().decode())

    assert "creator" not in collection_data

    if exclude_records:
        assert "records" not in collection_data
    else:
        assert "records" in collection_data
        assert len(collection_data["records"]) == 1

        record_data = collection_data["records"][0]

        assert "creator" not in record_data
        assert "files" in record_data
        assert record_data["links"] == []


def test_get_export_data_qr(dummy_collection, dummy_user):
    """Test if the collection QR code export works correctly."""
    assert get_export_data(dummy_collection, "qr", user=dummy_user).getvalue()


@pytest.mark.parametrize("metadata_only", [True, False])
@pytest.mark.parametrize("exclude_records", [True, False])
def test_get_export_data_ro_crate(
    metadata_only,
    exclude_records,
    dummy_collection,
    dummy_record,
    dummy_file,
    dummy_user,
    new_record,
):
    """Test if the collection RO-Crate export works correctly."""
    record = new_record()

    add_link(dummy_collection.records, dummy_record, user=dummy_user)
    add_link(dummy_collection.records, record, user=dummy_user)

    export_filter = {"records": exclude_records, "metadata_only": metadata_only}
    export_data = get_export_data(
        dummy_collection, "ro-crate", export_filter=export_filter, user=dummy_user
    )

    if metadata_only:
        metadata = json.loads(export_data.read().decode())
        graph = metadata["@graph"]

        if exclude_records:
            assert len(graph) == 2
        else:
            assert len(graph) == 7

        assert graph[0]["@id"] == "ro-crate-metadata.json"
        assert graph[0]["@type"] == "CreativeWork"

        assert graph[1]["@id"] == "./"
        assert graph[1]["@type"] == ["Dataset"]

        if exclude_records:
            assert len(graph[1]["hasPart"]) == 0
        else:
            assert len(graph[1]["hasPart"]) == 2

        if not exclude_records:
            # The newly created record should be included first due to its timestamp.
            assert graph[2]["@id"] == f"./{record.identifier}/"
            assert graph[2]["@type"] == "Dataset"
            assert len(graph[2]["hasPart"]) == 1

            assert graph[3]["@id"] == f"./{record.identifier}/{record.identifier}.json"
            assert graph[3]["@type"] == "File"

            # And the dummy record with it's file afterwards.
            assert graph[4]["@id"] == f"./{dummy_record.identifier}/"
            assert graph[4]["@type"] == "Dataset"
            assert len(graph[4]["hasPart"]) == 2

            assert (
                graph[5]["@id"]
                == f"./{dummy_record.identifier}/{dummy_record.identifier}.json"
            )
            assert graph[5]["@type"] == "File"

            assert (
                graph[6]["@id"]
                == f"./{dummy_record.identifier}/files/{dummy_file.name}"
            )
            assert graph[6]["@type"] == "File"

    else:
        data = functools.reduce(lambda acc, val: acc + val, export_data)

        with zipfile.ZipFile(BytesIO(data)) as ro_crate:
            namelist = ro_crate.namelist()

            if exclude_records:
                assert len(namelist) == 1
            else:
                assert len(namelist) == 4

                root_dir = dummy_collection.identifier
                filepaths = [
                    os.path.join(root_dir, "ro-crate-metadata.json"),
                    os.path.join(
                        root_dir, record.identifier, f"{record.identifier}.json"
                    ),
                    os.path.join(
                        root_dir,
                        dummy_record.identifier,
                        f"{dummy_record.identifier}.json",
                    ),
                    os.path.join(
                        root_dir, dummy_record.identifier, "files", dummy_file.name
                    ),
                ]

                for filepath in filepaths:
                    assert filepath in ro_crate.namelist()

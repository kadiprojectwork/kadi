# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.exceptions import KadiPermissionError
from kadi.lib.permissions.core import add_role
from kadi.lib.resources.utils import add_link
from kadi.lib.tags.models import Tag
from kadi.modules.collections.core import create_collection
from kadi.modules.collections.core import delete_collection
from kadi.modules.collections.core import link_collections
from kadi.modules.collections.core import purge_collection
from kadi.modules.collections.core import restore_collection
from kadi.modules.collections.core import update_collection
from kadi.modules.collections.models import Collection
from kadi.modules.collections.models import CollectionState
from kadi.modules.collections.models import CollectionVisibility


def test_create_collection(dummy_template, dummy_user):
    """Test if collections are created correctly."""
    collection = create_collection(
        creator=dummy_user,
        identifier="test",
        title="test",
        description="# test",
        visibility=CollectionVisibility.PRIVATE,
        record_template=dummy_template.id,
        tags=["test"],
    )

    assert Collection.query.filter_by(identifier="test").one() == collection
    assert collection.plain_description == "test"
    assert collection.record_template == dummy_template
    assert collection.revisions.count() == 1
    assert Tag.query.filter_by(name="test").one()
    assert dummy_user.roles.filter_by(
        name="admin", object="collection", object_id=collection.id
    ).one()


def test_update_collection(dummy_collection, dummy_template, dummy_user):
    """Test if collections are updated correctly."""
    update_collection(
        dummy_collection,
        description="# test",
        record_template=dummy_template.id,
        tags=["test"],
        user=dummy_user,
    )

    assert dummy_collection.plain_description == "test"
    assert dummy_collection.record_template == dummy_template
    assert dummy_collection.revisions.count() == 2
    assert Tag.query.filter_by(name="test").one()


def test_delete_collection(dummy_collection, dummy_user):
    """Test if collections are deleted correctly."""
    delete_collection(dummy_collection, user=dummy_user)

    assert dummy_collection.state == CollectionState.DELETED
    assert dummy_collection.revisions.count() == 2


def test_restore_collection(dummy_collection, dummy_user):
    """Test if collections are restored correctly."""
    delete_collection(dummy_collection, user=dummy_user)
    restore_collection(dummy_collection, user=dummy_user)

    assert dummy_collection.state == CollectionState.ACTIVE
    assert dummy_collection.revisions.count() == 3


def test_purge_collection(
    db,
    dummy_collection,
    dummy_record,
    dummy_template,
    dummy_user,
    new_collection,
    new_user,
):
    """Test if collections are purged correctly."""
    user = new_user()
    collection = new_collection(record_template=dummy_template.id)

    add_role(user, "collection", collection.id, "member")
    add_link(collection.records, dummy_record, user=dummy_user)
    link_collections(collection, dummy_collection, user=dummy_user)
    db.session.commit()

    purge_collection(collection)

    assert Collection.query.get(collection.id) is None
    # Only the system role should remain.
    assert user.roles.count() == 1


def test_link_collections(dummy_user, new_collection, new_user):
    """Test if collections are linked together correctly."""
    collection_1 = new_collection()
    collection_2 = new_collection()
    collection_3 = new_collection()

    # Try to link the collection to itself.
    assert not link_collections(collection_1, collection_1, user=dummy_user)

    # Try linking without suitable permissions.
    with pytest.raises(KadiPermissionError):
        link_collections(collection_1, collection_2, user=new_user())

    # Link all three collections.
    assert link_collections(collection_1, collection_2, user=dummy_user)
    assert link_collections(collection_2, collection_3, user=dummy_user)

    # Try to link a collection that already has a parent.
    assert not link_collections(collection_1, collection_2, user=dummy_user)
    # Try to create a cyclic relationship.
    assert not link_collections(collection_3, collection_1, user=dummy_user)

# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.favorites.models import Favorite
from kadi.lib.permissions.models import Role
from kadi.lib.web import url_for
from kadi.modules.accounts.models import UserState
from kadi.modules.accounts.utils import save_user_image
from tests.utils import check_api_response


def test_preview_user_image(client, dummy_image, dummy_user, user_session):
    """Test the internal "api.preview_user_image" endpoint."""
    save_user_image(dummy_user, dummy_image)

    with user_session():
        response = client.get(url_for("api.preview_user_image", id=dummy_user.id))
        check_api_response(response, content_type="image/jpeg")
        response.close()


def test_get_favorite_resources(client, dummy_record, dummy_user, user_session):
    """Test the internal "api.get_favorite_resources" endpoint."""
    object_name = "record"

    Favorite.create(user=dummy_user, object=object_name, object_id=dummy_record.id)

    with user_session():
        response = client.get(
            url_for("api.get_favorite_resources", resource_type="test")
        )

        check_api_response(response, status_code=404)

        response = client.get(
            url_for("api.get_favorite_resources", resource_type=object_name)
        )

        check_api_response(response)
        assert response.get_json()["items"][0]["id"] == dummy_record.id


def test_select_users(client, user_session):
    """Test the internal "api.select_users" endpoint."""
    with user_session():
        response = client.get(url_for("api.select_users"))
        check_api_response(response)


def test_change_system_role(client, dummy_user, user_session):
    """Test the internal "api.change_system_role" endpoint."""
    dummy_user.is_sysadmin = True
    new_role = "admin"

    with user_session():
        response = client.patch(
            url_for("api.change_system_role", id=dummy_user.id), json={"name": new_role}
        )

        check_api_response(response, status_code=204)
        assert (
            dummy_user.roles.filter(Role.object.is_(None), Role.object_id.is_(None))
            .one()
            .name
            == new_role
        )


def test_toggle_user_state(client, dummy_user, user_session):
    """Test the internal "api.toggle_user_state" endpoint."""
    dummy_user.is_sysadmin = True

    with user_session():
        response = client.patch(url_for("api.toggle_user_state", id=dummy_user.id))

        check_api_response(response, status_code=204)
        assert dummy_user.state == UserState.INACTIVE


def test_toggle_user_sysadmin(client, dummy_user, user_session):
    """Test the internal "api.toggle_user_sysadmin" endpoint."""
    dummy_user.is_sysadmin = True

    with user_session():
        response = client.patch(url_for("api.toggle_user_sysadmin", id=dummy_user.id))

        check_api_response(response, status_code=204)
        assert not dummy_user.is_sysadmin


def test_delete_user(client, dummy_user, user_session):
    """Test the internal "api.delete_user" endpoint."""
    dummy_user.is_sysadmin = True

    with user_session():
        response = client.delete(url_for("api.delete_user", id=dummy_user.id))

        check_api_response(response, status_code=204)
        assert dummy_user.state == UserState.DELETED


def test_get_users(api_client, dummy_access_token):
    """Test the "api.get_users" endpoint."""
    response = api_client(dummy_access_token).get(url_for("api.get_users"))
    check_api_response(response)


def test_get_current_user(api_client, dummy_access_token, dummy_user):
    """Test the "api.get_current_user" endpoint."""
    response = api_client(dummy_access_token).get(url_for("api.get_current_user"))

    check_api_response(response)
    assert response.get_json()["id"] == dummy_user.id


def test_get_user_by_identity(api_client, dummy_access_token, dummy_user):
    """Test the "api.get_user_by_identity" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for(
            "api.get_user_by_identity",
            identity_type=dummy_user.identity.type,
            username=dummy_user.identity.username,
        )
    )

    check_api_response(response)
    assert response.get_json()["id"] == dummy_user.id


@pytest.mark.parametrize(
    "endpoint,args",
    [
        ("get_user", {}),
        ("get_user_identities", {}),
        ("get_user_records", {}),
        ("get_user_records", {"shared": True}),
        ("get_user_collections", {}),
        ("get_user_collections", {"shared": True}),
        ("get_user_templates", {}),
        ("get_user_templates", {"shared": True}),
        ("get_user_groups", {}),
        ("get_user_groups", {"common": True}),
    ],
)
def test_get_user_endpoints(endpoint, args, api_client, dummy_access_token, dummy_user):
    """Test the remaining "api.get_user*" endpoints."""
    response = api_client(dummy_access_token).get(
        url_for(f"api.{endpoint}", id=dummy_user.id, **args)
    )
    check_api_response(response)

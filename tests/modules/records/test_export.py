# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import functools
import os
import zipfile
from io import BytesIO

import pytest
from flask import json

from kadi.modules.records.export import filter_extras
from kadi.modules.records.export import get_export_data
from kadi.modules.records.links import create_record_link


@pytest.mark.parametrize(
    "extras,filter,result",
    [
        (
            [{"key": "foo", "type": "str", "value": None}],
            {"bar": {}},
            [{"key": "foo", "type": "str", "value": None}],
        ),
        (
            [{"key": "foo", "type": "str", "value": None}],
            {"foo": {"bar": {}}},
            [{"key": "foo", "type": "str", "value": None}],
        ),
        (
            [{"key": "foo", "type": "str", "value": None}],
            {"foo": {}},
            [],
        ),
        (
            [
                {
                    "key": "foo",
                    "type": "dict",
                    "value": [{"key": "bar", "type": "str", "value": None}],
                }
            ],
            {"foo": {"bar": {}}},
            [
                {
                    "key": "foo",
                    "type": "dict",
                    "value": [],
                }
            ],
        ),
        (
            [
                {
                    "key": "foo",
                    "type": "list",
                    "value": [{"type": "str", "value": None}],
                }
            ],
            {"foo": {"0": {}}},
            [
                {
                    "key": "foo",
                    "type": "list",
                    "value": [],
                }
            ],
        ),
    ],
)
def test_filter_extras(extras, filter, result):
    """Test if exported extras are filtered correctly."""
    assert filter_extras(extras, filter) == result


def test_get_export_data_json(dummy_collection, dummy_record, dummy_user, new_record):
    """Test if the record JSON export works correctly."""
    create_record_link(
        name="out", record_from=dummy_record, record_to=new_record(), creator=dummy_user
    )
    create_record_link(
        name="in", record_from=new_record(), record_to=dummy_record, creator=dummy_user
    )

    export_filter = {"user": True, "links": "in"}
    json_data = get_export_data(
        dummy_record, "json", export_filter=export_filter, user=dummy_user
    )

    assert json_data is not None

    record_data = json.loads(json_data.read().decode())

    assert "creator" not in record_data
    assert "files" in record_data
    assert "links" in record_data
    assert len(record_data["links"]) == 1

    link_data = record_data["links"][0]

    assert "creator" not in link_data

    linked_record_data = link_data["record_to"]

    assert "creator" not in linked_record_data
    assert "files" not in linked_record_data
    assert "links" not in linked_record_data


def test_get_export_data_pdf(dummy_file, dummy_record, dummy_user, new_record):
    """Test if the record PDF export works correctly."""
    record = new_record()
    create_record_link(
        name="test", record_from=dummy_record, record_to=record, creator=dummy_user
    )

    # Check handling of unicode characters and some extra metadata with a date.
    dummy_record.description = b"\xf0\x9f\x90\xb1".decode()
    dummy_record.extras = [
        {
            "key": "test",
            "type": "dict",
            "value": [
                {
                    "key": "test",
                    "type": "date",
                    "value": "2020-01-01T12:34:56.789000+00:00",
                }
            ],
        }
    ]

    assert get_export_data(dummy_record, "pdf", user=dummy_user).getvalue()


def test_get_export_data_qr(dummy_record, dummy_user):
    """Test if the record QR code export works correctly."""
    assert get_export_data(dummy_record, "qr", user=dummy_user).getvalue()


@pytest.mark.parametrize("metadata_only", [True, False])
def test_get_export_data_ro_crate(
    metadata_only, dummy_file, dummy_record, dummy_user, new_record
):
    """Test if the record RO-Crate export works correctly."""
    create_record_link(
        name="test",
        record_from=dummy_record,
        record_to=new_record(),
        creator=dummy_user,
    )

    export_filter = {"metadata_only": metadata_only}
    export_data = get_export_data(
        dummy_record, "ro-crate", export_filter=export_filter, user=dummy_user
    )

    if metadata_only:
        metadata = json.loads(export_data.read().decode())
        graph = metadata["@graph"]

        assert len(graph) == 5

        assert graph[0]["@id"] == "ro-crate-metadata.json"
        assert graph[0]["@type"] == "CreativeWork"

        assert graph[1]["@id"] == "./"
        assert graph[1]["@type"] == ["Dataset"]
        assert len(graph[1]["hasPart"]) == 1

        assert graph[2]["@id"] == f"./{dummy_record.identifier}/"
        assert graph[2]["@type"] == "Dataset"
        assert len(graph[2]["hasPart"]) == 2
        assert len(graph[2]["mentions"]) == 1

        assert (
            graph[3]["@id"]
            == f"./{dummy_record.identifier}/{dummy_record.identifier}.json"
        )
        assert graph[3]["@type"] == "File"

        assert graph[4]["@id"] == f"./{dummy_record.identifier}/files/{dummy_file.name}"
        assert graph[4]["@type"] == "File"

    else:
        data = functools.reduce(lambda acc, val: acc + val, export_data)

        with zipfile.ZipFile(BytesIO(data)) as ro_crate:
            namelist = ro_crate.namelist()

            assert len(namelist) == 3

            root_dir = dummy_record.identifier
            filepaths = [
                os.path.join(root_dir, "ro-crate-metadata.json"),
                os.path.join(
                    root_dir, dummy_record.identifier, f"{dummy_record.identifier}.json"
                ),
                os.path.join(
                    root_dir, dummy_record.identifier, "files", dummy_file.name
                ),
            ]

            for filepath in filepaths:
                assert filepath in ro_crate.namelist()

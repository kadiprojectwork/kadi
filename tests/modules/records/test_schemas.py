# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from marshmallow import ValidationError

from kadi.modules.records.models import File
from kadi.modules.records.models import FileState
from kadi.modules.records.schemas import FileSchema


def test_file_schema_duplicate_file(dummy_record, dummy_user):
    """Test if checking for duplicate files in the "FileSchema" works correctly."""
    file = File.create(
        creator=dummy_user,
        record=dummy_record,
        name="test",
        size=0,
        state=FileState.ACTIVE,
    )
    payload = {"name": "test"}

    FileSchema(only=["name"]).load(payload)
    FileSchema(only=["name"], record=dummy_record, previous_file=file).load(payload)

    with pytest.raises(ValidationError):
        FileSchema(only=["name"], record=dummy_record).load(payload)

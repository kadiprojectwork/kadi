# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from werkzeug.datastructures import MultiDict

from kadi.lib.resources.utils import add_link
from kadi.modules.records.forms import EditFileForm
from kadi.modules.records.forms import NewRecordForm
from kadi.modules.records.models import File
from kadi.modules.records.models import FileState


def test_new_record_form_record(
    dummy_collection, dummy_license, dummy_user, new_collection, new_record, new_user
):
    """Test if prefilling a "NewRecordForm" with a record works correctly."""
    record = new_record(type="test", license=dummy_license.name, tags=["test"])

    user = new_user()
    # This collection should not appear in the linked collections in the form.
    collection = new_collection(creator=user)
    add_link(record.collections, collection, user=user)
    add_link(record.collections, dummy_collection, user=dummy_user)

    form = NewRecordForm(record=record, user=dummy_user)

    tag = record.tags.first().name

    assert form.identifier.data == record.identifier
    assert form.type.initial == (record.type, record.type)
    assert form.license.initial == (dummy_license.name, dummy_license.title)
    assert form.tags.initial == [(tag, tag)]
    assert form.collections.initial == [
        (dummy_collection.id, f"@{dummy_collection.identifier}")
    ]
    assert form.roles.initial == []


def test_new_record_form_template(
    dummy_collection, dummy_license, dummy_user, new_collection, new_template, new_user
):
    """Test if prefilling a "NewRecordForm" with a record template works correctly."""

    # This collection should not appear in the linked collections in the form.
    collection = new_collection(creator=new_user())

    template = new_template(
        data={
            "identifier": "test",
            "type": "test",
            "license": dummy_license.name,
            "tags": ["test"],
            "collections": [dummy_collection.id, collection.id],
            "roles": [],
        }
    )

    form = NewRecordForm(template=template, user=dummy_user)

    template_type = template.data["type"]
    tag = template.data["tags"][0]

    assert form.identifier.data == template.data["identifier"]
    assert form.type.initial == (template_type, template_type)
    assert form.license.initial == (dummy_license.name, dummy_license.title)
    assert form.tags.initial == [(tag, tag)]
    assert form.collections.initial == [
        (dummy_collection.id, f"@{dummy_collection.identifier}")
    ]
    assert form.roles.initial == []


def test_new_record_form_collection(dummy_collection, dummy_user):
    """Test if prefilling a "NewRecordForm" with a collection works correctly."""
    form = NewRecordForm(collection=dummy_collection, user=dummy_user)
    assert form.collections.initial == [
        (dummy_collection.id, f"@{dummy_collection.identifier}")
    ]


def test_file_form_validate_name(dummy_record, dummy_user):
    """Test if checking for duplicate files in the "EditFileForm" works correctly."""
    file = File.create(
        creator=dummy_user,
        record=dummy_record,
        name="test",
        size=0,
        state=FileState.ACTIVE,
    )
    File.create(
        creator=dummy_user,
        record=dummy_record,
        name="test2",
        size=0,
        state=FileState.ACTIVE,
    )

    form = EditFileForm(file=file, formdata=MultiDict({"name": "test"}))

    assert form.validate()

    form = EditFileForm(file=file, formdata=MultiDict({"name": "test2"}))

    assert not form.validate()
    assert "Name is already in use." in form.errors["name"]

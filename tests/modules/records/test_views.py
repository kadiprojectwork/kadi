# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import json

import kadi.lib.constants as const
from kadi.lib.permissions.models import Role
from kadi.lib.web import url_for
from kadi.modules.records.links import create_record_link
from kadi.modules.records.models import FileState
from kadi.modules.records.models import Record
from kadi.modules.records.models import RecordLink
from kadi.modules.records.models import RecordState
from kadi.modules.records.models import RecordVisibility
from tests.utils import check_view_response


def test_records(client, user_session):
    """Test the "records.records" endpoint."""
    with user_session():
        response = client.get(url_for("records.records"))
        check_view_response(response)


def test_new_record(client, user_session):
    """Test the "records.new_record" endpoint."""
    endpoint = url_for("records.new_record")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "identifier": "test",
                "title": "test",
                "visibility": RecordVisibility.PRIVATE,
            },
        )

        check_view_response(response, status_code=302)
        assert Record.query.filter_by(identifier="test").one()


def test_edit_record(client, dummy_record, user_session):
    """Test the "records.edit_record" endpoint."""
    endpoint = url_for("records.edit_record", id=dummy_record.id)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"identifier": "test"})

        check_view_response(response, status_code=302)
        assert dummy_record.identifier == "test"


def test_view_record(client, dummy_record, user_session):
    """Test the "records.view_record" endpoint."""
    with user_session():
        response = client.get(url_for("records.view_record", id=dummy_record.id))
        check_view_response(response)


@pytest.mark.parametrize("export_type", const.EXPORT_TYPES["record"])
def test_export_record(export_type, client, dummy_record, user_session):
    """Test the "records.export_record" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "records.export_record", id=dummy_record.id, export_type=export_type
            )
        )
        check_view_response(response)


def test_publish_record(client, dummy_record, user_session):
    """Test the "records.publish_record" endpoint."""
    endpoint = url_for("records.publish_record", id=dummy_record.id, provider="test")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response, status_code=404)

        response = client.post(endpoint)
        check_view_response(response, status_code=404)


def test_manage_links_records(
    client, dummy_collection, dummy_group, dummy_record, new_record, user_session
):
    """Test the "records" tab of the "records.manage_links" endpoint."""
    endpoint = url_for("records.manage_links", id=dummy_record.id, tab="records")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        record = new_record()
        response = client.post(
            endpoint,
            data={"name": "test", "record": record.id, "link_direction": "out"},
        )

        check_view_response(response, status_code=302)
        assert dummy_record.links_to[0].record_to == record


def test_manage_links_collections(
    client, dummy_collection, dummy_group, dummy_record, new_record, user_session
):
    """Test the "collections" tab of the "records.manage_links" endpoint."""
    endpoint = url_for("records.manage_links", id=dummy_record.id, tab="collections")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"collections": [dummy_collection.id]})

        check_view_response(response)
        assert dummy_record.collections.one() == dummy_collection


@pytest.mark.parametrize("direction", ["from", "to"])
def test_view_record_link(
    direction, client, dummy_record, dummy_user, new_record, user_session
):
    """Test the "records.view_record_link" endpoint."""
    record_link = create_record_link(
        name="test",
        record_from=dummy_record,
        record_to=new_record(),
        creator=dummy_user,
    )

    if direction == "from":
        record_id = record_link.record_from_id
    else:
        record_id = record_link.record_to_id

    with user_session():
        response = client.get(
            url_for(
                "records.view_record_link", link_id=record_link.id, record_id=record_id
            )
        )
        check_view_response(response)


@pytest.mark.parametrize("direction", ["from", "to"])
def test_edit_record_link(
    direction, client, dummy_record, dummy_user, new_record, user_session
):
    """Test the "records.edit_record_link" endpoint."""
    record_link = create_record_link(
        name="test",
        record_from=dummy_record,
        record_to=new_record(),
        creator=dummy_user,
    )

    if direction == "from":
        record_id = record_link.record_from_id
    else:
        record_id = record_link.record_to_id

    endpoint = url_for(
        "records.edit_record_link", link_id=record_link.id, record_id=record_id
    )

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"name": "test2"})

        check_view_response(response, status_code=302)
        assert record_link.name == "test2"


def test_manage_permissions(client, dummy_group, dummy_record, new_user, user_session):
    """Test the "records.manage_permissions" endpoint."""
    endpoint = url_for("records.manage_permissions", id=dummy_record.id)
    new_role = "member"
    user = new_user()

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "roles": json.dumps(
                    [
                        {
                            "subject_type": "user",
                            "subject_id": user.id,
                            "role": new_role,
                        },
                        {
                            "subject_type": "group",
                            "subject_id": dummy_group.id,
                            "role": new_role,
                        },
                    ]
                )
            },
        )

        check_view_response(response, status_code=302)
        assert user.roles.filter(
            Role.object == "record",
            Role.object_id == dummy_record.id,
            Role.name == new_role,
        ).one()
        assert dummy_group.roles.filter(
            Role.object == "record",
            Role.object_id == dummy_record.id,
            Role.name == new_role,
        ).one()


def test_add_files(client, dummy_record, user_session):
    """Test the "records.add_files" endpoint."""
    with user_session():
        response = client.get(url_for("records.add_files", id=dummy_record.id))
        check_view_response(response)


def test_view_file(client, dummy_file, dummy_record, user_session):
    """Test the "records.view_file" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "records.view_file", record_id=dummy_record.id, file_id=dummy_file.id
            )
        )
        check_view_response(response)


def test_edit_file_metadata(client, dummy_file, dummy_record, user_session):
    """Test the "records.edit_file_metadata" endpoint."""
    endpoint = url_for(
        "records.edit_file_metadata", record_id=dummy_record.id, file_id=dummy_file.id
    )

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"name": "test.txt"})

        check_view_response(response, status_code=302)
        assert dummy_file.name == "test.txt"


def test_view_record_revision(client, dummy_record, user_session):
    """Test the "records.view_record_revision" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "records.view_record_revision",
                record_id=dummy_record.id,
                revision_id=dummy_record.ordered_revisions.first().id,
            )
        )
        check_view_response(response)


def test_view_file_revision(client, dummy_file, dummy_record, user_session):
    """Test the "records.view_file_revision" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "records.view_file_revision",
                record_id=dummy_record.id,
                revision_id=dummy_file.ordered_revisions.first().id,
            )
        )
        check_view_response(response)


def test_delete_record(client, dummy_record, user_session):
    """Test the "records.delete_record" endpoint."""
    with user_session():
        response = client.post(url_for("records.delete_record", id=dummy_record.id))

        check_view_response(response, status_code=302)
        assert dummy_record.state == RecordState.DELETED


@pytest.mark.parametrize("direction", ["from", "to"])
def test_remove_record_link(
    direction, client, dummy_record, dummy_user, new_record, user_session
):
    """Test the "records.remove_record_link" endpoint."""
    record_link = create_record_link(
        name="test",
        record_from=dummy_record,
        record_to=new_record(),
        creator=dummy_user,
    )

    if direction == "from":
        record_id = record_link.record_from_id
    else:
        record_id = record_link.record_to_id

    with user_session():
        response = client.post(
            url_for(
                "records.remove_record_link",
                link_id=record_link.id,
                record_id=record_id,
            )
        )

        check_view_response(response, status_code=302)
        assert not RecordLink.query.all()


def test_delete_file(client, dummy_file, dummy_record, user_session):
    """Test the "records.delete_file" endpoint."""
    with user_session():
        response = client.post(
            url_for(
                "records.delete_file", record_id=dummy_record.id, file_id=dummy_file.id
            )
        )

        check_view_response(response, status_code=302)
        assert dummy_file.state == FileState.INACTIVE

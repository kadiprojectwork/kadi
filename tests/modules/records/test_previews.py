# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import zipfile
from io import BytesIO

import kadi.lib.constants as const
from kadi.lib.web import url_for
from kadi.modules.records.previews import get_preview_data


def test_get_preview_data(dummy_file, new_file):
    """Test some of the built-in preview types."""
    zip_data = BytesIO()

    with zipfile.ZipFile(zip_data, mode="w", compression=zipfile.ZIP_DEFLATED) as f:
        f.writestr("test.txt", "test")

    zip_file = new_file(
        file_data=zip_data.getbuffer(), magic_mimetype="application/zip"
    )
    assert get_preview_data(zip_file) == (
        "archive",
        [{"name": "test.txt", "size": 4, "is_dir": False}],
    )

    csv_file = new_file(file_data=b"f;o;o\r\n1;2;3\n\n", mimetype=const.MIMETYPE_CSV)
    assert get_preview_data(csv_file) == (
        "csv",
        {
            "rows": [["f", "o", "o"], ["1", "2", "3"]],
            "encoding": "ascii",
            "has_header": True,
        },
    )

    assert get_preview_data(dummy_file) == (
        "image",
        url_for(
            "api.preview_file", record_id=dummy_file.record_id, file_id=dummy_file.id
        ),
    )

    json_file = new_file(file_data=b'{"foo": "bar"}')
    assert get_preview_data(json_file) == (
        "json",
        {"json": {"foo": "bar"}, "encoding": "ascii"},
    )

    markdown_file = new_file(
        file_data=b"**foo**\n*bar*\r\nbaz", mimetype="text/markdown"
    )
    assert get_preview_data(markdown_file) == (
        "markdown",
        {"lines": ["**foo**", "*bar*", "baz"], "encoding": "ascii"},
    )

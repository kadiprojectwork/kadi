# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from collections import OrderedDict
from io import BytesIO

from kadi.lib.security import hash_value


def initiate_upload(
    client,
    endpoint,
    name="test.txt",
    description="",
    size=None,
    mimetype="text/plain",
    checksum=None,
    storage_type=None,
    file_data=b"",
    replace_file=False,
):
    """Initiate a new chunked upload."""
    size = size if size is not None else len(file_data)
    checksum = checksum if checksum is not None else hash_value(file_data, alg="md5")

    payload = {
        "description": description,
        "size": size,
        "mimetype": mimetype,
        "checksum": checksum,
    }

    if storage_type is not None:
        payload["storage"] = {"storage_type": storage_type}

    if replace_file:
        response = client.put(endpoint, json=payload)
    else:
        payload["name"] = name
        response = client.post(endpoint, json=payload)

    return response


def upload_chunk(client, endpoint, index=0, size=None, chunk_data=b"", checksum=None):
    """Upload a chunk of an existing chunked upload."""
    size = size if size is not None else len(chunk_data)
    checksum = checksum if checksum is not None else hash_value(chunk_data, alg="md5")

    return client.put(
        endpoint,
        data={
            "checksum": checksum,
            "index": str(index),
            "size": str(size),
            "blob": (BytesIO(chunk_data), f"chunk_{index}"),
        },
        content_type="multipart/form-data",
    )


def upload_file(
    client,
    endpoint,
    name="test.txt",
    description="",
    size=None,
    mimetype="text/plain",
    checksum=None,
    storage_type=None,
    file_data=b"",
    replace_file=False,
):
    """Directly upload a file."""
    size = size if size is not None else len(file_data)
    checksum = checksum if checksum is not None else hash_value(file_data, alg="md5")

    form_data = OrderedDict()

    if storage_type is not None:
        form_data["storage_type"] = storage_type

    form_data.update(
        [
            ("replace_file", str(replace_file)),
            ("mimetype", mimetype),
            ("checksum", checksum),
            ("description", description),
            ("name", name),
            ("size", size),
            ("blob", (BytesIO(file_data), "file")),
        ]
    )

    return client.post(endpoint, data=form_data, content_type="multipart/form-data")

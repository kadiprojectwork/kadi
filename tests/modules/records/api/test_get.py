# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

import pytest

import kadi.lib.constants as const
from .utils import initiate_upload
from .utils import upload_chunk
from kadi.lib.storage.local import create_default_local_storage
from kadi.lib.web import url_for
from kadi.modules.records.links import create_record_link
from kadi.modules.records.models import TemporaryFile
from kadi.modules.records.models import TemporaryFileState
from tests.utils import check_api_response


def _get_export_mimetype(export_type):
    if export_type == "pdf":
        return "application/pdf"
    if export_type == "qr":
        return "image/png"
    if export_type == "ro-crate":
        return "application/zip"

    return const.MIMETYPE_JSON


@pytest.mark.parametrize("export_type", const.EXPORT_TYPES["record"])
@pytest.mark.parametrize("download", [True, False])
def test_get_record_export_internal(
    export_type, download, client, dummy_record, user_session
):
    """Test the internal "api.get_record_export_internal" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "api.get_record_export_internal",
                id=dummy_record.id,
                export_type=export_type,
                download=download,
            )
        )

        content_type = const.MIMETYPE_JSON

        if download:
            content_type = _get_export_mimetype(export_type)

        check_api_response(response, content_type=content_type)
        response.close()


def test_get_record_links_graph(client, dummy_record, user_session):
    """Test the internal "api.get_record_links_graph" endpoint."""
    with user_session():
        response = client.get(url_for("api.get_record_links_graph", id=dummy_record.id))
        check_api_response(response)


def test_get_file_preview(client, dummy_file, dummy_record, user_session):
    """Test the internal "api.get_file_preview" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "api.get_file_preview", record_id=dummy_record.id, file_id=dummy_file.id
            )
        )
        check_api_response(response)


def test_preview_file(client, dummy_file, dummy_record, user_session):
    """Test the internal "api.preview_file" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "api.preview_file", record_id=dummy_record.id, file_id=dummy_file.id
            )
        )
        check_api_response(response, content_type="image/jpeg")
        response.close()


def test_download_temporary_file(client, db, dummy_record, dummy_user, user_session):
    """Test the internal "api.download_temporary_file" endpoint."""
    file_data = 10 * b"x"
    storage = create_default_local_storage()

    temporary_file = TemporaryFile.create(
        creator=dummy_user,
        record=dummy_record,
        name="test.txt",
        size=len(file_data),
        state=TemporaryFileState.ACTIVE,
    )
    db.session.commit()

    filepath = storage.create_filepath(str(temporary_file.id))
    storage.save(filepath, BytesIO(file_data))

    with user_session():
        response = client.get(
            url_for(
                "api.download_temporary_file",
                record_id=dummy_record.id,
                temporary_file_id=temporary_file.id,
            )
        )
        check_api_response(response, content_type="text/plain; charset=utf-8")
        response.close()


@pytest.mark.parametrize(
    "endpoint",
    [
        "select_records",
        "select_record_types",
        "select_mimetypes",
        "select_files",
        "select_link_names",
    ],
)
def test_select_endpoints(endpoint, client, user_session):
    """Test the internal "api.select_*" endpoints."""
    with user_session():
        response = client.get(url_for(f"api.{endpoint}"))
        check_api_response(response)


def test_get_records(api_client, dummy_access_token):
    """Test the "api.get_records" endpoint."""
    response = api_client(dummy_access_token).get(url_for("api.get_records"))
    check_api_response(response)


def test_get_record_by_identifier(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_record_by_identifier" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_record_by_identifier", identifier=dummy_record.identifier)
    )
    check_api_response(response)


def test_get_record_link(
    api_client, dummy_access_token, dummy_record, dummy_user, new_record
):
    """Test the "api.get_record_link" endpoint."""
    record_link = create_record_link(
        name="test",
        record_from=dummy_record,
        record_to=new_record(),
        creator=dummy_user,
    )

    response = api_client(dummy_access_token).get(
        url_for(
            "api.get_record_link", record_id=dummy_record.id, link_id=record_link.id
        )
    )
    check_api_response(response)


def test_get_record_revision(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_record_revision" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for(
            "api.get_record_revision",
            record_id=dummy_record.id,
            revision_id=dummy_record.ordered_revisions.first().id,
        )
    )
    check_api_response(response)


@pytest.mark.parametrize("export_type", const.EXPORT_TYPES["record"])
def test_get_record_export(export_type, client, dummy_record, user_session):
    """Test the "api.get_record_export" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "api.get_record_export", id=dummy_record.id, export_type=export_type
            )
        )
        content_type = _get_export_mimetype(export_type)

        check_api_response(response, content_type=content_type)
        response.close()


@pytest.mark.parametrize(
    "endpoint,args",
    [
        ("get_record", {}),
        ("get_record_links", {"direction": "out"}),
        ("get_record_links", {"direction": "in"}),
        ("get_record_collections", {}),
        ("get_record_user_roles", {}),
        ("get_record_group_roles", {}),
        ("get_record_revisions", {}),
    ],
)
def test_get_record_endpoints(
    endpoint, args, api_client, dummy_access_token, dummy_record
):
    """Test the remaining "api.get_record*" endpoints."""
    response = api_client(dummy_access_token).get(
        url_for(f"api.{endpoint}", id=dummy_record.id, **args)
    )
    check_api_response(response)


def test_get_files(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_files" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_files", id=dummy_record.id)
    )
    check_api_response(response)


def test_get_file(api_client, dummy_access_token, dummy_file, dummy_record):
    """Test the "api.get_file" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_file", record_id=dummy_record.id, file_id=dummy_file.id)
    )
    check_api_response(response)


def test_get_file_by_name(api_client, dummy_access_token, dummy_file, dummy_record):
    """Test the "api.get_file_by_name" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for(
            "api.get_file_by_name", record_id=dummy_record.id, filename=dummy_file.name
        )
    )
    check_api_response(response)


def test_download_file(api_client, dummy_access_token, dummy_file, dummy_record):
    """Test the "api.download_file" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.download_file", record_id=dummy_record.id, file_id=dummy_file.id)
    )
    check_api_response(response, content_type="image/jpeg")
    response.close()


def test_download_files(api_client, dummy_access_token, dummy_record):
    """Test the "api.download_files" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.download_files", id=dummy_record.id)
    )
    check_api_response(response, content_type="application/zip")
    response.close()


def test_get_file_revisions(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_file_revisions" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_file_revisions", id=dummy_record.id)
    )
    check_api_response(response)


def test_get_file_revision(api_client, dummy_access_token, dummy_file, dummy_record):
    """Test the "api.get_file_revision" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for(
            "api.get_file_revision",
            record_id=dummy_record.id,
            revision_id=dummy_file.ordered_revisions.first().id,
        )
    )
    check_api_response(response)


def test_get_uploads(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_uploads" endpoint."""
    response = api_client(dummy_access_token).get(
        url_for("api.get_uploads", id=dummy_record.id)
    )
    check_api_response(response)


def test_get_upload_status(api_client, dummy_access_token, dummy_record):
    """Test the "api.get_upload_status" endpoint."""
    client = api_client(dummy_access_token)
    file_data = 10 * b"x"

    response = initiate_upload(
        client, url_for("api.new_upload", id=dummy_record.id), file_data=file_data
    )
    data = response.get_json()

    response = client.get(data["_links"]["status"])
    data = response.get_json()

    check_api_response(response)
    assert "_meta" not in data

    upload_chunk(
        client, data["_actions"]["upload_chunk"], chunk_data=file_data, index=0
    )
    client.post(data["_actions"]["finish_upload"])
    response = client.get(data["_links"]["status"])
    data = response.get_json()

    check_api_response(response)
    assert "_meta" in data and "file" in data["_meta"]

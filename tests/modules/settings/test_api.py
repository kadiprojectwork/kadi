# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.api.models import AccessToken
from kadi.lib.web import url_for
from tests.utils import check_api_response


def test_get_access_tokens(client, user_session):
    """Test the internal "api.get_access_tokens" endpoint."""
    with user_session():
        response = client.get(url_for("api.get_access_tokens"))
        check_api_response(response)


def test_remove_access_token(client, db, dummy_access_token, user_session):
    """Test the internal "api.remove_access_token" endpoint."""
    access_token = AccessToken.get_by_token(dummy_access_token)

    with user_session():
        response = client.delete(url_for("api.remove_access_token", id=access_token.id))
        db.session.commit()

        check_api_response(response, status_code=204)
        assert not AccessToken.query.all()

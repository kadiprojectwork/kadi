# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import get_flashed_messages
from flask import json

import kadi.lib.constants as const
from kadi.lib.api.models import AccessToken
from kadi.lib.config.core import get_user_config
from kadi.lib.oauth.core import create_oauth2_token
from kadi.lib.oauth.models import OAuth2Token
from kadi.lib.web import url_for
from tests.utils import check_view_response


def test_edit_profile(client, dummy_user, user_session):
    """Test the "settings.edit_profile" endpoint."""
    endpoint = url_for("settings.edit_profile")

    with user_session():
        response = client.post(url_for("settings.edit_profile", action="confirm_email"))

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert "A confirmation email has been sent." in get_flashed_messages()

        response = client.get(endpoint)
        check_view_response(response)

        new_email = "test@example.com"
        dummy_user.identity.email_confirmed = True
        response = client.post(endpoint, data={"email": new_email})

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert dummy_user.identity.email == new_email
        assert not dummy_user.identity.email_confirmed


def test_change_password(client, dummy_user, user_session):
    """Test the "settings.change_password" endpoint."""
    endpoint = url_for("settings.change_password")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "password": dummy_user.identity.username,
                "new_password": "test1234",
                "new_password2": "test1234",
            },
        )

        check_view_response(response, status_code=302)
        assert dummy_user.identity.check_password("test1234")


def test_manage_preferences_customization(client, dummy_user, user_session):
    """Test the "customization" tab of the "settings.manage_preferences" endpoint."""
    endpoint = url_for("settings.manage_preferences", tab="customization")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={"home_layout": json.dumps(const.USER_CONFIG_HOME_LAYOUT_DEFAULT)},
        )

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert (
            get_user_config(const.USER_CONFIG_HOME_LAYOUT, user=dummy_user)
            == const.USER_CONFIG_HOME_LAYOUT_DEFAULT
        )


def test_manage_tokens(client, user_session):
    """Test the "settings.manage_tokens" endpoint."""
    endpoint = url_for("settings.manage_tokens")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"name": "test"})

        check_view_response(response)
        assert AccessToken.query.filter_by(name="test").one()


def test_manage_services(monkeypatch, client, db, dummy_user, user_session):
    """Test the "settings.manage_services" endpoint."""
    monkeypatch.setattr(
        "kadi.modules.settings.views.has_oauth2_providers", lambda: True
    )

    provider_name = "test"
    endpoint = url_for("settings.manage_services")

    create_oauth2_token(name=provider_name, access_token="test", user=dummy_user)
    db.session.commit()

    assert OAuth2Token.query.one()

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(f"{endpoint}?disconnect={provider_name}")
        db.session.commit()

        check_view_response(response, status_code=302)
        assert not OAuth2Token.query.all()


def test_oauth2_login(client, user_session):
    """Test the "settings.oauth2_login" endpoint."""
    with user_session():
        response = client.get(url_for("settings.oauth2_login", provider="test"))
        check_view_response(response, status_code=404)


def test_oauth2_authorize(client, user_session):
    """Test the "settings.oauth2_authorize" endpoint."""
    with user_session():
        response = client.get(url_for("settings.oauth2_authorize", provider="test"))
        check_view_response(response, status_code=404)

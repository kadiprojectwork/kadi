# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import json
from jsonschema import Draft202012Validator
from jsonschema import FormatChecker

from kadi.modules.templates.export import get_export_data
from kadi.modules.templates.models import TemplateType


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
def test_get_export_data_json(template_type, dummy_user, new_template):
    """Test if the template JSON export works correctly."""
    template = new_template(type=template_type)

    extra_1 = {
        "key": "test",
        "type": "dict",
        "value": [{"key": "test", "type": "str", "value": None}],
    }
    filtered_extra_1 = {"key": "test", "type": "dict", "value": []}

    if template_type == TemplateType.RECORD:
        template.data["extras"] = [extra_1]
    else:
        template.data = [extra_1]

    export_filter = {
        "user": True,
        "extras": {"test": {"test": {}}},
    }
    json_data = get_export_data(
        template, "json", export_filter=export_filter, user=dummy_user
    )

    assert json_data is not None

    template_data = json.loads(json_data.read().decode())

    if template_type == TemplateType.RECORD:
        assert "creator" not in template_data
        assert template_data["data"]["extras"] == [filtered_extra_1]
    else:
        assert template_data["data"] == [filtered_extra_1]


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
@pytest.mark.parametrize(
    "extra,data",
    [
        # String value with required validation and options.
        (
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"required": True, "options": ["test1", "test2"]},
            },
            {"test": "test1"},
        ),
        # Integer value with a range validation.
        (
            {
                "type": "int",
                "key": "test",
                "value": 1,
                "unit": None,
                "validation": {"range": {"min": 1, "max": 2}},
            },
            {"test": 1},
        ),
        # Optional float value.
        (
            {
                "type": "float",
                "key": "test",
                "value": None,
                "unit": "test",
            },
            {"test": None},
        ),
        # Regular boolean value.
        (
            {
                "key": "test",
                "type": "bool",
                "value": None,
            },
            {"test": True},
        ),
        # Regular date value.
        (
            {
                "key": "test",
                "type": "date",
                "value": None,
            },
            {"test": "2020-01-01T12:34:56.789Z"},
        ),
        # Nested dictionary value.
        (
            {
                "key": "test",
                "type": "dict",
                "value": [
                    {"key": "test1", "type": "str", "value": None},
                    {"key": "test2", "type": "bool", "value": None},
                ],
            },
            {"test": {"test1": "test", "test2": True}},
        ),
        # Nested list value.
        (
            {
                "key": "test",
                "type": "list",
                "value": [
                    {"type": "str", "value": None},
                    {"type": "bool", "value": None},
                ],
            },
            {"test": ["test", True]},
        ),
    ],
)
def test_get_export_data_json_schema(
    template_type, extra, data, dummy_user, new_template
):
    """Test if the template JSON Schema export works correctly."""
    template = new_template(type=template_type)

    if template_type == TemplateType.RECORD:
        template.data["extras"] = [extra]
    else:
        template.data = [extra]

    json_data = get_export_data(template, "json-schema", user=dummy_user)

    assert json_data is not None

    json_schema = json.loads(json_data.read().decode())

    assert "$schema" in json_schema

    if template_type == TemplateType.RECORD:
        data = {"identifier": "test", "title": "test", "extras": data}

    validator = Draft202012Validator(schema=json_schema, format_checker=FormatChecker())
    assert validator.is_valid(instance=data)

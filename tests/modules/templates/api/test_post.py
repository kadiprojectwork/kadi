# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.web import url_for
from kadi.modules.templates.core import delete_template
from kadi.modules.templates.models import Template
from kadi.modules.templates.models import TemplateState
from kadi.modules.templates.models import TemplateType
from tests.modules.utils import check_api_post_subject_resource_role
from tests.utils import check_api_response


@pytest.mark.parametrize(
    "template_type,template_data",
    [(TemplateType.RECORD, {}), (TemplateType.EXTRAS, [])],
)
def test_new_template(template_type, template_data, api_client, dummy_access_token):
    """Test the "api.new_template" endpoint."""
    response = api_client(dummy_access_token).post(
        url_for("api.new_template"),
        json={
            "type": template_type,
            "identifier": "test",
            "title": "test",
            "data": template_data,
        },
    )

    check_api_response(response, status_code=201)
    assert Template.query.filter_by(identifier="test").one()


def test_add_template_user_role(
    api_client, db, dummy_access_token, dummy_template, new_user
):
    """Test the "api.add_template_user_role" endpoint."""
    client = api_client(dummy_access_token)
    endpoint = url_for("api.add_template_user_role", id=dummy_template.id)

    check_api_post_subject_resource_role(
        db, client, endpoint, new_user(), dummy_template
    )


def test_add_template_group_role(
    api_client, db, dummy_access_token, dummy_template, dummy_group
):
    """Test the "api.add_template_group_role" endpoint."""
    client = api_client(dummy_access_token)
    endpoint = url_for("api.add_template_group_role", id=dummy_template.id)

    check_api_post_subject_resource_role(
        db, client, endpoint, dummy_group, dummy_template
    )


def test_restore_template(api_client, dummy_access_token, dummy_template, dummy_user):
    """Test the "api.restore_template" endpoint."""
    delete_template(dummy_template, user=dummy_user)

    response = api_client(dummy_access_token).post(
        url_for("api.restore_template", id=dummy_template.id)
    )

    check_api_response(response)
    assert dummy_template.state == TemplateState.ACTIVE


def test_purge_template(api_client, dummy_access_token, dummy_template, dummy_user):
    """Test the "api.purge_template" endpoint."""
    delete_template(dummy_template, user=dummy_user)

    response = api_client(dummy_access_token).post(
        url_for("api.purge_template", id=dummy_template.id)
    )

    check_api_response(response, status_code=204)
    assert Template.query.get(dummy_template.id) is None

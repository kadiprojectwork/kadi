# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from tests.modules.utils import check_api_patch_subject_resource_role
from tests.utils import check_api_response


def test_toggle_favorite_template(client, dummy_template, dummy_user, user_session):
    """Test the internal "api.toggle_favorite_template" endpoint."""
    with user_session():
        response = client.patch(
            url_for("api.toggle_favorite_template", id=dummy_template.id)
        )

        check_api_response(response, status_code=204)
        assert dummy_template.is_favorite(user=dummy_user)


def test_edit_template(api_client, dummy_access_token, dummy_template):
    """Test the "api.edit_template" endpoint."""
    response = api_client(dummy_access_token).patch(
        url_for("api.edit_template", id=dummy_template.id), json={"identifier": "test"}
    )

    check_api_response(response)
    assert dummy_template.identifier == "test"


def test_change_template_user_role(
    api_client, db, dummy_access_token, dummy_template, dummy_user, new_user
):
    """Test the "api.change_template_user_role" endpoint."""
    user = new_user()
    client = api_client(dummy_access_token)
    endpoint = url_for(
        "api.change_template_user_role", template_id=dummy_template.id, user_id=user.id
    )
    change_creator_endpoint = url_for(
        "api.change_template_user_role",
        template_id=dummy_template.id,
        user_id=dummy_user.id,
    )

    check_api_patch_subject_resource_role(
        db,
        client,
        endpoint,
        user,
        dummy_template,
        change_creator_endpoint=change_creator_endpoint,
    )


def test_change_template_group_role(
    api_client, db, dummy_access_token, dummy_group, dummy_template
):
    """Test the "api.change_template_group_role" endpoint."""
    client = api_client(dummy_access_token)
    endpoint = url_for(
        "api.change_template_group_role",
        template_id=dummy_template.id,
        group_id=dummy_group.id,
    )

    check_api_patch_subject_resource_role(
        db, client, endpoint, dummy_group, dummy_template
    )

# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from kadi.modules.notifications.models import Notification
from tests.utils import check_api_response


def test_get_notifications(client, user_session):
    """Test the internal "api.get_notifications" endpoint."""
    with user_session():
        response = client.get(url_for("api.get_notifications"))
        check_api_response(response)


def test_dismiss_notification(client, db, dummy_user, user_session):
    """Test the internal "api.dismiss_notification" endpoint."""
    notification = Notification.create(user=dummy_user, name="test")
    db.session.commit()

    with user_session():
        response = client.delete(
            url_for("api.dismiss_notification", id=notification.id)
        )
        db.session.commit()

        check_api_response(response, status_code=204)
        assert not Notification.query.all()

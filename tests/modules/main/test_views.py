# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

import kadi.lib.constants as const
from kadi.lib.config.core import set_sys_config
from kadi.lib.web import url_for
from tests.utils import check_view_response


def test_index(client, user_session):
    """Test the "main.index" endpoint."""
    endpoint = url_for("main.index")

    response = client.get(endpoint)
    check_view_response(response)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)


def test_about(client):
    """Test the "main.about" endpoint."""
    response = client.get(url_for("main.about"))
    check_view_response(response)


def test_help(client):
    """Test the "main.help" endpoint."""
    response = client.get(url_for("main.help"))
    check_view_response(response)


@pytest.mark.parametrize(
    "endpoint,config_item",
    [
        ("terms_of_use", const.SYS_CONFIG_TERMS_OF_USE),
        ("privacy_policy", const.SYS_CONFIG_PRIVACY_POLICY),
        ("legal_notice", const.SYS_CONFIG_LEGAL_NOTICE),
    ],
)
def test_legals(endpoint, config_item, client):
    """Test the endpoints related to displaying legal notices."""
    set_sys_config(config_item, "Test")

    response = client.get(url_for(f"main.{endpoint}"))
    check_view_response(response)


@pytest.mark.parametrize("enforce_legals", [True, False])
def test_request_legals_acceptance(enforce_legals, client, dummy_user, user_session):
    """Test the "main.request_legals_acceptance" endpoint."""
    set_sys_config(const.SYS_CONFIG_TERMS_OF_USE, "Test")
    set_sys_config(const.SYS_CONFIG_ENFORCE_LEGALS, enforce_legals)

    endpoint = url_for("main.request_legals_acceptance")

    assert not dummy_user.legals_accepted

    with user_session():
        response = client.get(endpoint)

        if not enforce_legals:
            check_view_response(response, status_code=302)
        else:
            check_view_response(response)

            response = client.post(endpoint, data={"accept_legals": True})

            check_view_response(response, status_code=302)
            assert dummy_user.legals_accepted

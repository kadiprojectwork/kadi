# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
from contextlib import contextmanager
from contextlib import redirect_stderr
from functools import partial

import pytest
from flask import current_app
from flask import g
from flask.testing import FlaskClient
from flask_migrate import downgrade
from flask_migrate import upgrade

import kadi.lib.constants as const
from kadi.app import create_app
from kadi.ext.db import db as database
from kadi.lib.permissions.utils import initialize_system_role
from kadi.lib.search.elasticsearch import Elasticsearch
from kadi.lib.web import url_for


@pytest.fixture
def get_fixture(request):
    """Fixture to create a factory to get other fixture values."""

    def _get_fixture(name):
        return request.getfixturevalue(name)

    return _get_fixture


@pytest.fixture(scope="session")
def _app(tmp_path_factory):
    """Fixture to create an application using the testing environment.

    Passes a general, temporary "STORAGE_PATH" to the app factory, since a valid path is
    required at app creation time even in test environments.
    """
    config = {"STORAGE_PATH": str(tmp_path_factory.mktemp("storage"))}
    return create_app(environment=const.ENV_TESTING, config=config)


@pytest.fixture(autouse=True)
def _app_context(_app):
    """Fixture to push an application context.

    Executed automatically for each test.
    """
    with _app.app_context():
        yield _app


@pytest.fixture(autouse=True)
def _patch_config_storage_paths(monkeypatch, tmp_path):
    """Fixture to patch the local storage paths in the config.

    Will automatically patch the "STORAGE_PATH" and "MISC_UPLOADS_PATH" of the current
    application to different temporary locations for each test. The storage path will
    also be set as the root directory for the default local storage registered in the
    application.

    Executed automatically for each test.
    """
    storage_path = os.path.join(tmp_path, "storage")
    misc_uploads_path = os.path.join(tmp_path, "uploads")

    monkeypatch.setitem(current_app.config, "STORAGE_PATH", storage_path)
    monkeypatch.setitem(current_app.config, "MISC_UPLOADS_PATH", misc_uploads_path)

    monkeypatch.setattr(
        current_app.config["STORAGES"][const.STORAGE_TYPE_LOCAL],
        "_root_directory",
        storage_path,
    )


@pytest.fixture
def request_context(_app):
    """Fixture to push a dummy request context."""
    with _app.test_request_context():
        yield


@pytest.fixture
def user_session(client, dummy_user):
    """Fixture to create a persistent session with a logged-in local user.

    If no username and password are given, the logged-in user will be the dummy user.
    """

    @contextmanager
    def _user_session(username=None, password=None):
        if username is None or password is None:
            username = password = dummy_user.identity.username

        with client:
            client.post(
                url_for(
                    "accounts.login_with_provider",
                    provider=const.AUTH_PROVIDER_TYPE_LOCAL,
                ),
                data={"username": username, "password": password},
            )

            yield

            client.get(url_for("accounts.logout"))

    return _user_session


@pytest.fixture
def clear_user(_app):
    """Completely clear a logged-in user from the application context.

    This workaround is sometimes necessary in order to re-trigger one of the user
    loaders when performing multiple requests within a single test, as each test uses a
    single application context.
    """
    attr = "_login_user"

    def _clear_user():
        if hasattr(g, attr):
            delattr(g, attr)

    return _clear_user


@pytest.fixture
def client(_app):
    """Fixture to create a client for testing regular requests."""
    return _app.test_client()


@pytest.fixture
def api_client(_app, clear_user):
    """Fixture to create a client for testing API request.

    The client needs to be instantiated with an access token which will be used for all
    subsequent requests.
    """

    class _APIClient(FlaskClient):
        def __init__(self, token):
            super().__init__(_app, _app.response_class, use_cookies=False)
            self.token = token

        def __getattribute__(self, attr):
            if attr in ["get", "post", "put", "patch", "delete"]:
                request_method = partial(
                    super().__getattribute__(attr),
                    headers={"Authorization": f"Bearer {self.token}"},
                )

                # Always clear the user for API requests using an access token, as these
                # requests are supposed to be stateless. Note that this is done before
                # the request is actually performed.
                clear_user()

                return request_method

            return super().__getattribute__(attr)

    return _APIClient


@pytest.fixture(scope="session")
def register_view(_app):
    """Fixture to create a factory for registering test view functions.

    The URL corresponding to the view function will be returned when registering it.
    Note that each view function will be added to the regular URL map of the app, which
    is valid for the entire test session.
    """
    count = 0

    def _register_view(func, api_endpoint=False, **options):
        nonlocal count
        count += 1

        endpoint = f"_test_{count}"
        rule = f"/{endpoint}" if not api_endpoint else f"/api/{endpoint}"

        _app.add_url_rule(rule, endpoint, func, **options)

        return url_for(endpoint)

    return _register_view


@pytest.fixture(scope="session")
def _db(_app):
    # pylint: disable=unspecified-encoding
    migrations_path = _app.config["MIGRATIONS_PATH"]

    with _app.app_context():
        with open(os.devnull, mode="w") as f:
            with redirect_stderr(f):
                upgrade(directory=migrations_path, revision="head")

        for role_name in const.SYSTEM_ROLES:
            initialize_system_role(role_name)

        database.session.commit()

    yield database

    with _app.app_context():
        # Close the session explicitly, otherwise the database can hang.
        database.session.close()

        with open(os.devnull, mode="w") as f:
            with redirect_stderr(f):
                downgrade(directory=migrations_path, revision="base")


@pytest.fixture(autouse=True)
def db(_db, monkeypatch):
    """Fixture to create and initialize a temporary database.

    All changes will be rolled back after each test.
    """
    monkeypatch.setattr(_db.session, "commit", _db.session.flush)

    return _db


@pytest.fixture(autouse=True)
def _elasticsearch(monkeypatch):
    """Fixture to patch Elasticsearch to return empty results on search.

    Will patch the Elasticsearch instance in all relevant modules where it is used.

    Executed automatically for each test.
    """

    class _Elasticsearch(Elasticsearch):
        def search(self, *args, **kwargs):
            # pylint: disable=missing-function-docstring
            return {"hits": {"hits": {}}}

    monkeypatch.setattr("kadi.lib.search.core.es", _Elasticsearch())

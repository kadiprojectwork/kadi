# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

import pytest
from PIL import Image

from kadi.lib.api.core import create_access_token
from kadi.lib.api.models import AccessToken
from kadi.lib.licenses.models import License
from kadi.lib.revisions.core import create_revision
from kadi.lib.security import hash_value
from kadi.modules.accounts.providers import LocalProvider
from kadi.modules.collections.core import create_collection
from kadi.modules.groups.core import create_group
from kadi.modules.records.core import create_record
from kadi.modules.records.models import File
from kadi.modules.records.models import FileState
from kadi.modules.records.models import Upload
from kadi.modules.records.models import UploadType
from kadi.modules.templates.core import create_template
from kadi.modules.templates.models import TemplateType


@pytest.fixture
def new_user(db):
    """Fixture to create a factory for creating new local users."""
    count = 0

    def _new_user(
        username=None, password=None, email=None, displayname=None, system_role=None
    ):
        nonlocal count
        count += 1

        username = username if username is not None else f"user_{count}"
        password = password if password is not None else username
        email = email if email is not None else f"{username}@example.com"
        displayname = displayname if displayname is not None else username

        identity = LocalProvider.register(
            username=username,
            password=password,
            email=email,
            displayname=displayname,
            system_role=system_role,
        )
        return identity.user

    return _new_user


@pytest.fixture
def dummy_user(db):
    """Fixture to create a local dummy user."""
    username = password = "dummy-user"

    identity = LocalProvider.register(
        username=username,
        password=password,
        email=f"{username}@example.com",
        displayname="Dummy user",
    )
    return identity.user


@pytest.fixture
def new_record(db, dummy_user):
    """Fixture to create a factory for creating new records."""
    count = 0

    def _new_record(creator=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        return create_record(
            creator=creator,
            identifier=f"record_{count}",
            title=f"Record {count}",
            **kwargs,
        )

    return _new_record


@pytest.fixture
def dummy_record(db, dummy_user):
    """Fixture to create a dummy record."""
    return create_record(
        creator=dummy_user, identifier="dummy-record", title="Dummy record"
    )


@pytest.fixture
def new_file(db, dummy_record, dummy_image, dummy_user):
    """Fixture to create a factory for creating new files."""
    count = 0

    def _new_file(creator=None, record=None, file_data=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user
        record = record if record is not None else dummy_record
        file_data = BytesIO(file_data) if file_data is not None else dummy_image

        buffer = file_data.getbuffer()

        file = File.create(
            creator=creator,
            record=record,
            name=f"dummy_{count}.jpg",
            size=len(buffer),
            checksum=hash_value(buffer, alg="md5"),
            state=FileState.ACTIVE,
            **kwargs,
        )
        db.session.flush()

        storage = file.storage

        filepath = storage.create_filepath(str(file.id))
        storage.save(filepath, file_data)

        if file.magic_mimetype is None:
            file.magic_mimetype = storage.get_mimetype(filepath)

        create_revision(file, user=creator)

        db.session.commit()
        return file

    return _new_file


@pytest.fixture
def dummy_file(db, dummy_image, dummy_record, dummy_user):
    """Fixture to create a local dummy file.

    The content of the file consists of the content of the "dummy_image" fixture.
    """
    buffer = dummy_image.getbuffer()

    file = File.create(
        creator=dummy_user,
        record=dummy_record,
        name="dummy.jpg",
        size=len(buffer),
        checksum=hash_value(buffer, alg="md5"),
        mimetype="image/jpeg",
        magic_mimetype="image/jpeg",
        state=FileState.ACTIVE,
    )
    db.session.flush()

    storage = file.storage

    filepath = storage.create_filepath(str(file.id))
    storage.save(filepath, dummy_image)

    create_revision(file, user=dummy_user)

    db.session.commit()
    return file


@pytest.fixture
def new_upload(db, dummy_record, dummy_user):
    """Fixture to create a factory for creating new uploads."""
    count = 0

    def _new_upload(
        creator=None,
        record=None,
        size=0,
        upload_type=UploadType.CHUNKED,
        file=None,
        **kwargs,
    ):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user
        record = record if record is not None else dummy_record

        upload = Upload.create(
            creator=creator,
            record=record,
            size=size,
            file=file,
            upload_type=upload_type,
            name=f"dummy_{count}.txt",
            **kwargs,
        )

        db.session.commit()
        return upload

    return _new_upload


@pytest.fixture
def dummy_upload(db, dummy_record, dummy_user):
    """Fixture to create a chunked dummy upload."""
    upload = Upload.create(
        creator=dummy_user,
        record=dummy_record,
        size=0,
        name="dummy.txt",
        upload_type=UploadType.CHUNKED,
    )

    db.session.commit()
    return upload


@pytest.fixture
def new_collection(db, dummy_user):
    """Fixture to create a factory for creating new collections."""
    count = 0

    def _new_collection(creator=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        return create_collection(
            creator=creator,
            identifier=f"collection_{count}",
            title=f"Collection {count}",
            **kwargs,
        )

    return _new_collection


@pytest.fixture
def dummy_collection(db, dummy_user):
    """Fixture to create a dummy collection."""
    return create_collection(
        creator=dummy_user, identifier="dummy-collection", title="Dummy collection"
    )


@pytest.fixture
def new_group(db, dummy_user):
    """Fixture to create a factory for creating new groups."""
    count = 0

    def _new_group(creator=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        return create_group(
            creator=creator,
            identifier=f"group_{count}",
            title=f"Group {count}",
            **kwargs,
        )

    return _new_group


@pytest.fixture
def dummy_group(db, dummy_user):
    """Fixture to create a dummy group."""
    return create_group(
        creator=dummy_user, identifier="dummy-group", title="Dummy group"
    )


@pytest.fixture
def new_template(db, dummy_user):
    """Fixture to create a factory for creating new templates."""
    count = 0

    def _new_template(creator=None, type=TemplateType.RECORD, data=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        if data is None:
            if type == TemplateType.RECORD:
                data = {}
            if type == TemplateType.EXTRAS:
                data = []

        return create_template(
            creator=creator,
            type=type,
            data=data,
            identifier=f"template_{count}",
            title=f"Template {count}",
            **kwargs,
        )

    return _new_template


@pytest.fixture
def dummy_template(db, dummy_user):
    """Fixture to create a dummy record template."""
    return create_template(
        creator=dummy_user,
        type=TemplateType.RECORD,
        data={},
        identifier="dummy-template",
        title="Dummy template",
    )


@pytest.fixture
def new_access_token(db, dummy_user):
    """Fixture to create a factory for creating new access tokens."""

    def _new_token(user=None, name=None, token=None, **kwargs):
        user = user if user is not None else dummy_user
        name = name if name is not None else "token"
        token = token if token is not None else AccessToken.new_token()

        create_access_token(user=user, name=name, token=token, **kwargs)

        db.session.commit()
        return token

    return _new_token


@pytest.fixture
def dummy_access_token(db, dummy_user):
    """Fixture to create a dummy access token."""
    token = AccessToken.new_token()
    create_access_token(user=dummy_user, name="dummy-token", token=token)

    db.session.commit()
    return token


@pytest.fixture
def dummy_license(db):
    """Fixture to create a dummy license."""
    license = License.create(name="test", title="test")

    db.session.commit()
    return license


@pytest.fixture
def dummy_image():
    """Fixture to create an in-memory JPEG image with a single white pixel."""
    image = Image.new("RGB", size=(1, 1), color=(255, 255, 255))

    image_data = BytesIO()
    image.save(image_data, format="JPEG")
    image_data.seek(0)

    return image_data

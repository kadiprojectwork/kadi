variables:
  POSTGRES_DB: kadi
  POSTGRES_USER: kadi
  POSTGRES_PASSWORD: ""
  POSTGRES_HOST_AUTH_METHOD: trust

default:
  image: python:3.10-bullseye
  before_script:
    # Use the development environment when using the Kadi CLI.
    - export KADI_ENV=development
    # Always set up the Kadi config file, in case it is needed.
    - echo "SQLALCHEMY_DATABASE_URI='postgresql://kadi:@postgres:5432/kadi'" > /kadi.py
    - export KADI_CONFIG_FILE=/kadi.py
    # Retrieve the tox version to install from "requirements.dev.txt".
    - TOX_VERSION=$(grep -oP "tox==\K.*" requirements.dev.txt)
    # If pip is available, update it and install tox.
    - command -v pip > /dev/null && pip install -U pip && pip install tox==${TOX_VERSION}
    # If npm is available, install all packages listed in "package.json".
    - command -v npm > /dev/null && npm install

stages:
  - pre
  - test
  - deploy

pre-commit:
  stage: pre
  tags:
    - docker
  script:
    - tox -e pre-commit

pylint:
  stage: pre
  tags:
    - docker
  script:
    - tox -e pylint

check-db:
  stage: pre
  tags:
    - docker
  services:
    - postgres:15
  script:
    - tox -e check-db

npm-audit:
  stage: pre
  tags:
    - docker
  image: node:18-bullseye
  script:
    - npm run audit

test-py37:
  stage: test
  tags:
    - docker
  image: python:3.7-bullseye
  services:
    - postgres:15
  script:
    - tox -e py37
  coverage: '/TOTAL.*\s+(\d+%)/'

test-py38:
  stage: test
  tags:
    - docker
  image: python:3.8-bullseye
  services:
    - postgres:15
  script:
    - tox -e py38
  coverage: '/TOTAL.*\s+(\d+%)/'

test-py39:
  stage: test
  tags:
    - docker
  image: python:3.9-bullseye
  services:
    - postgres:15
  script:
    - tox -e py39
  coverage: '/TOTAL.*\s+(\d+%)/'

test-py310:
  stage: test
  tags:
    - docker
  services:
    - postgres:15
  script:
    - tox -e py310
  coverage: '/TOTAL.*\s+(\d+%)/'

test-i18n:
  stage: test
  tags:
    - docker
  script:
    - tox -e i18n

test-assets:
  stage: test
  tags:
    - docker
  image: node:18-bullseye
  script:
    - npm run build

test-docs:
  stage: test
  tags:
    - docker
  script:
    - tox -e docs

release-pypi:
  stage: deploy
  tags:
    - docker
  script:
    - ./bin/make_release.sh
  rules:
    - if: '$CI_PROJECT_NAMESPACE == "iam-cms" && $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/'

"""empty message

Revision ID: 2a5423581cd1
Revises: c4c2c0ef46fa, bb533034115f
Create Date: 2023-01-30 19:28:50.785066

"""
import sqlalchemy as sa
from alembic import op

import kadi.lib.migration_types


# revision identifiers, used by Alembic.
revision = "2a5423581cd1"
down_revision = ("c4c2c0ef46fa", "bb533034115f")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass

# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# pylint: disable=missing-function-docstring
import requests
from authlib.integrations.requests_client import OAuth2Session
from flask import Blueprint
from flask import render_template

from . import PLUGIN_NAME
from kadi.ext.db import db
from kadi.lib.oauth.utils import get_oauth2_token
from kadi.plugins import get_plugin_config
from kadi.plugins import hookimpl


bp = Blueprint(
    PLUGIN_NAME,
    __name__,
    url_prefix="/oauthclient",
    template_folder="templates",
    static_folder="static",
)


@hookimpl
def kadi_register_blueprints(app):
    app.register_blueprint(bp)


@hookimpl
def kadi_register_oauth2_providers(registry):
    plugin_config = get_plugin_config(PLUGIN_NAME)

    for instance in plugin_config:
        kadi_instance = plugin_config.get(instance)

        if kadi_instance is None:
            continue

        client_id = kadi_instance["client_id"]
        client_secret = kadi_instance["client_secret"]
        base_url = kadi_instance.get("base_url")
        api_base_url = kadi_instance.get("api_base_url") or "/api"
        authorize_url = (
            kadi_instance.get("authorize_url") or "/oauth2server/oauth/authorize"
        )
        access_token_url = (
            kadi_instance.get("token_url") or "/oauth2server/oauth/access_token"
        )

        registry.register(
            name=instance,
            client_id=client_id,
            client_secret=client_secret,
            api_base_url=f"{base_url}{api_base_url}",
            authorize_url=f"{base_url}{authorize_url}",
            access_token_url=f"{base_url}{access_token_url}",
            access_token_params=None,
        )


@hookimpl
def kadi_get_oauth2_providers():
    plugin_config = get_plugin_config(PLUGIN_NAME)

    elements = list()
    results = dict()

    for instance in plugin_config:
        kadi_instance = plugin_config.get(instance)
        if kadi_instance is None:
            continue

        if isinstance(kadi_instance, dict):
            elements.append(
                {
                    "name": instance,
                    "client_id": kadi_instance.get("client_id"),
                    "title": kadi_instance.get("title"),
                    "website": kadi_instance.get("base_url"),
                    "description": render_template(
                        "oauthclient/description_oauth.html"
                    ),
                }
            )

    results["name"] = "kadi_instances"
    results["list"] = elements

    return results


@hookimpl
def kadi_get_config(provider_name):
    """
    Get the defined config values from the configuration file.
    """
    plugin_config = get_plugin_config(PLUGIN_NAME)

    for instance in plugin_config:
        kadi_instance = plugin_config.get(instance)

        if (
            kadi_instance is None
            or not isinstance(kadi_instance, dict)
            or instance != str(provider_name)
        ):
            continue
        return kadi_instance


@hookimpl
def kadi_revoke_refresh_token(provider_name, access_token, refresh_token):
    config = kadi_get_config(provider_name)

    if not isinstance(config, dict):
        return

    client = OAuth2Session(
        client_id=config.get("client_id"), client_secret=config.get("client_secret")
    )
    url = str(config.get("base_url")) + str(config.get("revoke_token_url"))

    headers = {"Authorization": "Bearer " + access_token}
    client.revoke_token(
        url,
        token=refresh_token,
        token_type_hint="refresh_token",
        headers=headers,
    )


@hookimpl
def kadi_get_records(provider, provider_name, user):
    """
    Get the records from another kadi provider.
    """
    oauth2_token = get_oauth2_token(
        provider_name,
        user=user,
        delete_on_error=True,
        auto_refresh=True,
        kadi_instance=True,
    )

    db.session.commit()

    user_data = dict()

    if oauth2_token is None:
        # Give the user a message to get a new access token in the connected services
        user_data[
            "message"
        ] = "Refresh Token is expired! Go to the connected services and request a new access and refresh token!"
        return user_data

    config = kadi_get_config(provider_name)
    if not isinstance(config, dict):
        return

    url_endpoint = str(provider.get("website")) + "/api/records"
    access_token = "Bearer " + oauth2_token.access_token
    headers = {"Authorization": access_token}
    resp = requests.get(url=url_endpoint, params={"_internal": True}, headers=headers)
    user_data = resp.json()

    provider_title = provider.get("title")
    for data in user_data["items"]:
        data["instance"] = provider_title

    results = dict()

    results["items"] = user_data["items"]
    results["total_items"] = user_data["_pagination"]["total_items"]
    return user_data["items"], user_data["_pagination"]["total_items"]

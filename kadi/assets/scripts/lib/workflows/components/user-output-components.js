/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

import {sockets, commonInputs, commonOutputs, BuiltinComponent} from 'core';

const type = 'user-output';
const menu = 'User Output';

const userOutputText = new BuiltinComponent(
  'UserOutputText',
  type,
  menu,
  [commonInputs.dep, {key: 'file', title: 'Text File', socket: sockets.str, required: true}],
  [commonOutputs.dep],
);

const userOutputWebView = new BuiltinComponent(
  'UserOutputWebView',
  type,
  menu,
  [
    commonInputs.dep,
    {key: 'description', title: 'Description', socket: sockets.str},
    {key: 'url', title: 'URL', socket: sockets.str},
  ],
  [commonOutputs.dep],
);

export default [userOutputText, userOutputWebView];

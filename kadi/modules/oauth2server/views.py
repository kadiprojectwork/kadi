# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from authlib.oauth2 import OAuth2Error
from flask import redirect
from flask import render_template
from flask import request
from flask import Response
from flask import url_for
from flask_babel import gettext as _
from flask_login import current_user
from flask_login import login_required

from kadi.ext.csrf import csrf
from kadi.ext.db import db
from kadi.lib.api.utils import get_access_token_scopes
from kadi.lib.web import flash_success
from kadi.lib.web import flash_warning
from kadi.modules.oauth2server.blueprint import bp
from kadi.modules.oauth2server.forms import AuthorizeForm
from kadi.modules.oauth2server.forms import CreateClientForm
from kadi.modules.oauth2server.forms import CreatedClientForm
from kadi.modules.oauth2server.forms import ShowClientForm
from kadi.modules.oauth2server.models.client import OAuth2ServerClient
from kadi.modules.oauth2server.models.scopes import Scope
from kadi.modules.oauth2server.server.server import authorization


@bp.route("/show_clients", methods=("GET", "POST"))
@login_required
def show_clients() -> str:
    """Endpoint to show all clients which were created by the current user.

    Returns:
        str: HTML page
    """
    clients = OAuth2ServerClient.query.filter_by(user_id=current_user.id).all()

    client_forms = []
    for client in clients:
        form = ShowClientForm(client)
        client_forms.append(form)

    return render_template("oauth2server/show_clients.html", client_forms=client_forms)


@bp.route("/create_client", methods=("GET", "POST"))
@login_required
def create_client() -> str:
    """Endpoint to register a new client.

    Returns:
        str: Page to create a new client or a redirect to show all registered clients
    """
    form = CreateClientForm()

    if not form.validate_on_submit():
        return render_template(
            "oauth2server/create_client.html",
            form=form,
            js_context={"all_scopes": Scope.query.all()},
        )

    selected_scopes = []
    for scope in request.form.getlist("scopes"):
        parts = scope.split(".", 1)
        if len(parts) != 2:
            continue

        object_name, action = parts
        selected_scopes.append(
            Scope.query.filter_by(object=object_name, action=action).first()
        )

    client_secret = OAuth2ServerClient.generate_new_client_secret()
    client = OAuth2ServerClient.create(
        client_name=form.client_name.data,
        client_uri=form.client_uri.data,
        client_secret=client_secret,
        allowed_grant_types=[("authorization_code"), ("refresh_token")],
        redirect_uri=form.redirect_uri.data,
        allowed_response_types="code",
        token_endpoint_authentication_method="client_secret_basic",
        user=current_user,
        scopes=selected_scopes,
    )
    db.session.commit()

    created_client_form = CreatedClientForm(client=client, client_secret=client_secret)

    flash_success(
        _(
            "Client created successfully. Please store your client secret somewhere safe!"
        )
    )
    return render_template(
        "oauth2server/show_created_client.html",
        form=created_client_form,
        js_context={
            "client_scopes": client.get_scopes(),
            "all_scopes": Scope.query.all(),
        },
    )


@bp.route("/delete_client/<string:client_id>", methods=("GET", "POST"))
@login_required
def delete_client(client_id: str) -> Response:
    """Endpoint to delete a registered client.

    Args:
        client_id (str): id of the client to be deleted

    Returns:
        Response: Redirect to show all registered clients with a success response
    """
    client = OAuth2ServerClient.query.filter_by(client_id=client_id).first()
    if client is None:
        flash_warning(_("Client could not be found."))
        return redirect(url_for("oauth2server.show_clients"))

    db.session.delete(client)
    db.session.commit()

    flash_success(_("Client deleted successfully."))
    return redirect(url_for("oauth2server.show_clients"))


@bp.route("/oauth/authorize", methods=["GET", "POST"])
@login_required
def authorize() -> str:
    """This endpoint returns a page to authorize the access to the user's data by a OAuth client if it is a request with the http method GET.
    If the form is submitted and validated successfully an authorization code will be returned.

    Returns:
        str: Authorization Page or Response
    """
    form = AuthorizeForm()

    # If the form validation is successfully completed, the user has given it's consent.
    # This is ensured through the form validator DataRequired.
    if form.validate_on_submit():
        return authorization.create_authorization_response(grant_user=current_user)

    try:
        grant = authorization.get_consent_grant(end_user=current_user)

        form.set_client_name(grant.client.client_name)
        return render_template(
            "oauth2server/authorize.html",
            form=form,
            js_context={
                "client_scopes": grant.client.get_scopes(),
                "all_scopes": Scope.query.all(),
            },
        )
    except OAuth2Error as error:
        return error.error


# Decorator @login_required can't be used because the post request has only an authorization code but no access token to login.
@bp.route("/oauth/access_token", methods=["POST"])
@csrf.exempt
def issue_token():
    """Interface to issue the OAuth access token after the authorization has been granted.
    The response includes an access token and a refresh token.

    Returns:
        Unkown: created access token including a refresh token
    """
    return authorization.create_token_response()


@bp.route("/oauth/access_token/refresh", methods=["POST"])
@csrf.exempt
def refresh_access_token():
    """Interface to issue a new access token for a given access token including a refresh token.

    Returns:
        Unkown: new access token including a new refresh token
    """
    return authorization.create_token_response()


@bp.route("/oauth/token/revoke", methods=["POST"])
@csrf.exempt
def revoke_acess_token():
    """Interface to revoke an access token or a refresh token.

    Returns:
        Unkown: Response for revoked token
    """
    return authorization.create_endpoint_response("revocation")

# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import datetime

from flask_babel import gettext as _
from flask_babel import lazy_gettext as _l
from wtforms.validators import DataRequired
from wtforms.validators import URL

from kadi.lib.conversion import lower
from kadi.lib.conversion import normalize
from kadi.lib.conversion import strip
from kadi.lib.forms import BooleanField
from kadi.lib.forms import KadiForm
from kadi.lib.forms import Length
from kadi.lib.forms import StringField
from kadi.lib.forms import SubmitField
from kadi.modules.oauth2server.models.client import OAuth2ServerClient


class CreateClientForm(KadiForm):
    """Form to to register new OAuth clients

    Args:
        KadiForm (KadiForm): Base form
    """

    client_name = StringField(
        _l("Client Name"),
        filters=[normalize],
        validators=[
            DataRequired(),
            Length(max=80),
        ],
        description=_l("The name with which the client should be saved"),
    )

    client_uri = StringField(
        _l("Client URI"),
        filters=[lower, strip],
        validators=[
            DataRequired(),
            Length(max=80),
            URL(),
        ],
        description=_l("The URI of the client"),
    )

    redirect_uri = StringField(
        _l("Redirect URI"),
        filters=[lower, strip],
        validators=[
            DataRequired(),
            Length(max=80),
            URL(),
        ],
        description=_l(
            "URL to which the request will be redirected after a successfull authentication"
        ),
    )

    submit = SubmitField(_l("Create Client"))


class CreatedClientForm(KadiForm):
    """A form to show the successfull created client with the client secret in plain text

    Args:
        KadiForm (KadiForm): Base form
    """

    client_secret = StringField(_l("Client Secret"))

    client_id = StringField(_l("Client ID"))

    client_name = StringField(_l("Client Name"))

    client_uri = StringField(_l("Client URI"))

    redirect_uri = StringField(_l("Redirect URI"))

    def __init__(
        self,
        *args,
        client_secret: str = None,
        client: OAuth2ServerClient = None,
        **kwargs
    ):
        """Initializes the form with the given data.

        Args:
            client_secret (str, optional): client secret. Defaults to None.
            client (OAuth2ServerClient, optional): client. Defaults to None.
        """
        super().__init__(*args, **kwargs)

        if client_secret is not None:
            self.set_client_secret(client_secret)

        if client is not None:
            self.set_client(client)

    def set_client_secret(self, client_secret):
        self._fields["client_secret"].data = client_secret

    def set_client(self, client):
        self._fields["client_id"].data = client.client_id
        self._fields["client_name"].data = client.client_metadata["client_name"]
        self._fields["client_uri"].data = client.client_metadata["client_uri"]
        self._fields["redirect_uri"].data = client.client_metadata["redirect_uris"]


class ShowClientForm(KadiForm):
    """A form to use to show existing OAuth clients

    Args:
        KadiForm (KadiForm): Base form
    """

    client_name = StringField(_l("Client Name"))

    client_uri = StringField(_l("Client URI"))

    redirect_uri = StringField(_l("Redirect URI"))

    client_id = StringField(_l("Client ID"))

    client_id_issued_at = StringField(_l("Client ID Issued At"))

    def __init__(self, client: OAuth2ServerClient, *args, **kwargs):
        """Initializes the form with the given data.

        Args:
            client (OAuth2ServerClient): client instance
        """
        super().__init__(*args, **kwargs)

        self._fields["client_name"].data = client.client_metadata["client_name"]
        self._fields["client_uri"].data = client.client_metadata["client_uri"]
        self._fields["redirect_uri"].data = client.client_metadata["redirect_uris"]
        self._fields["client_id"].data = client.client_id
        self._fields["client_id_issued_at"].data = datetime.datetime.fromtimestamp(
            client.client_id_issued_at
        ).strftime("%B  %d, %Y %H:%M:%S")


class AuthorizeForm(KadiForm):
    """Form to authorize the data access by the user

    Args:
        KadiForm (KadiForm): base
    """

    client_name = StringField(_l("Client Name"))

    consent = BooleanField(
        _l("Authorize application to use these scopes?"),
        validators=[DataRequired()],
    )

    submit = SubmitField(_l("Grant Authorization"))

    def __init__(self, *args, client_name: str = None, **kwargs):
        """Initializes the form with the given data.

        Args:
            client_name (str, optional): Client name. Defaults to None.
            expires_at (str, optional): Expiration of the client secret. Defaults to None.
        """
        super().__init__(*args, **kwargs)

        if client_name is not None:
            self.set_client_name(client_name)

    def set_client_name(self, client_name: str) -> None:
        """Sets the client name to the corresponding input field

        Args:
            client_name (str): client name
        """
        self._fields["client_name"].data = client_name

# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from dataclasses import dataclass

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy.orm import declarative_mixin
from sqlalchemy.orm import declared_attr

from kadi.ext.db import db
from kadi.lib.utils import SimpleReprMixin

# TODO: Move to suited location


@dataclass
class Scope(db.Model, SimpleReprMixin):
    """Scope model with all objects and actions used in the application.
    The annotation '@dataclass' is used to make a instance of the scope
    model serializable."""

    id: int
    object: str
    action: str

    __tablename__ = "scope"

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the scope, auto incremented."""

    object = db.Column(db.Text, nullable=False)
    """The object the action of the scope relates to."""

    action = db.Column(db.Text, nullable=False)
    """The action the scope allows to do related to its object."""

    oauth_client_scopes = db.relationship(
        "OAuth2ServerClientScopes", back_populates="scope"
    )

    oauth_authorization_code_scopes = db.relationship(
        "OAuth2ServerAuthorizationCodeScopes", back_populates="scope"
    )

    oauth_token_scopes = db.relationship(
        "OAuth2ServerTokenScopes", back_populates="scope"
    )

    def row2dict(row):
        return {c.name: str(getattr(row, c.name)) for c in row.__table__.columns}

    @classmethod
    def create(cls, object, action):
        scope = cls(object=object, action=action)

        db.session.add(scope)

        return scope

    @classmethod
    def initialize(cls):
        created_scopes = cls.query.all()
        if len(created_scopes) > 0:
            return

        cls.create("collection", "create")
        cls.create("collection", "read")
        cls.create("collection", "link")
        cls.create("collection", "update")
        cls.create("collection", "permissions")
        cls.create("collection", "delete")
        cls.create("group", "create")
        cls.create("group", "read")
        cls.create("group", "update")
        cls.create("group", "members")
        cls.create("group", "delete")
        cls.create("misc", "manage_trash")
        cls.create("record", "create")
        cls.create("record", "read")
        cls.create("record", "link")
        cls.create("record", "update")
        cls.create("record", "permissions")
        cls.create("record", "delete")
        cls.create("template", "create")
        cls.create("template", "read")
        cls.create("template", "update")
        cls.create("template", "permissions")
        cls.create("template", "delete")
        cls.create("user", "read")

        db.session.commit()


@declarative_mixin
class ScopeRelationMixin:
    """Mixin to build relationship tables from an entity to scope more easily.
    Unfortunately the relationship to the scopes model can't be defined in the mixin
    because the property 'back_populate's needs a relationship defined in the Scope model itself.
    The other possiblitiy to use the property 'back_ref' is deprecated."""

    @declared_attr
    def scope_id(cls):
        return Column("scope_id", ForeignKey("scope.id"))

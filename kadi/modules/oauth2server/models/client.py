# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import secrets
import time
from typing import List

from authlib.integrations.sqla_oauth2 import OAuth2ClientMixin
from sqlalchemy import PrimaryKeyConstraint
from werkzeug.security import gen_salt

from kadi.ext.db import db
from kadi.lib.security import hash_value
from kadi.modules.accounts.models import User
from kadi.modules.oauth2server.models.scopes import Scope
from kadi.modules.oauth2server.models.scopes import ScopeRelationMixin


class OAuth2ServerClient(db.Model, OAuth2ClientMixin):
    """Model to represent the registered clients of the OAuth server.

    Args:
        db (SQLAlchemy): database
        OAuth2ClientMixin (OAuth2ClientMixin): client mixin from authlib
    """

    __tablename__ = "oauth2_server_client"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))
    user = db.relationship("User")

    scopes = db.relationship(
        "OAuth2ServerClientScopes",
        lazy="dynamic",
        back_populates="client",
        cascade="all, delete-orphan",
    )
    authorization_codes = db.relationship(
        "OAuth2ServerAuthorizationCode",
        lazy="dynamic",
        back_populates="client",
        cascade="all, delete-orphan",
    )
    tokens = db.relationship(
        "OAuth2ServerToken",
        lazy="dynamic",
        back_populates="client",
        cascade="all, delete-orphan",
    )

    def check_client_secret(self, client_secret: str) -> bool:
        """Validates the client secrets by comparing their hashes.

        Args:
            client_secret (str): client secret

        Returns:
            bool: whether the secret matches the client's actual secret
        """
        return secrets.compare_digest(self.client_secret, hash_value(client_secret))

    def get_scopes(self):
        """Returns the client's scopes

        Returns:
            Scope.Array: Array including all found scopes
        """
        return (
            self.scopes.join(Scope, OAuth2ServerClientScopes.scope_id == Scope.id)
            .with_entities(Scope)
            .all()
        )

    @classmethod
    def generate_new_client_secret(cls) -> str:
        """Generates a client secret

        Returns:
            str: client secret
        """
        return gen_salt(48)

    @classmethod
    def create(
        cls,
        client_name: str,
        client_uri: str,
        client_secret: str,
        allowed_grant_types: List[str],
        redirect_uri: str,
        allowed_response_types: str,
        scopes: List[Scope],
        token_endpoint_authentication_method: str,
        user: User,
    ) -> "OAuth2ServerClient":
        """Adds the OAuth client to the database.

        Args:
            client_name (str): client name
            client_uri (str): client URI
            client_secret (str): client secret
            allowed_grant_types (list): allowed grant types
            redirect_uri (str): redirect URI
            allowed_response_types (str): allowed response types
            scopes (list): associated scopes
            token_endpoint_authentication_method (str): authentication method
            user (User): User

        Returns:
            OAuth2ServerClient: instance
        """

        client = cls(
            client_id=gen_salt(24),
            client_id_issued_at=int(time.time()),
            user_id=user.id,
        )

        client_metadata = {
            "client_name": client_name,
            "client_uri": client_uri,
            "grant_types": allowed_grant_types,
            "redirect_uris": redirect_uri,
            "response_types": allowed_response_types,
            "token_endpoint_auth_method": token_endpoint_authentication_method,
        }
        client.set_client_metadata(client_metadata)
        client.client_secret = hash_value(client_secret)

        db.session.add(client)

        for scope in scopes:
            OAuth2ServerClientScopes.create(client=client, scope=scope)

        return client


class OAuth2ServerClientScopes(db.Model, ScopeRelationMixin):
    """Model to represent scopes of the oauth client.

    Args:
        db (SQLAlchemy): database
        ScopeRelationMixin (ScopeRelationMixin): scope mixin
    """

    __tablename__ = "oauth2_server_client_scope"

    client_id = db.Column(
        db.Integer, db.ForeignKey("oauth2_server_client.id"), nullable=False
    )
    """The ID of the oauth client the scope belongs to."""

    scope = db.relationship("Scope", back_populates="oauth_client_scopes")

    client = db.relationship(
        "OAuth2ServerClient",
        back_populates="scopes",
    )

    __table_args__ = (
        PrimaryKeyConstraint(
            "scope_id", "client_id", name="oauth2_server_client_scope_pk"
        ),
    )

    @classmethod
    def create(
        cls, client: OAuth2ServerClient, scope: Scope
    ) -> "OAuth2ServerClientScopes":
        """Adds an association of the scope to the client and adds it to the database.

        Args:
            client (OAuth2ServerClient): client
            scope (Scope): scope to be added

        Returns:
            OAuth2ServerClientScopes: model instance
        """
        associated_scope = cls(client=client, scope=scope)

        db.session.add(associated_scope)

        return associated_scope

# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from authlib.oauth2.rfc6749 import grants
from flask import Flask

from kadi.ext.db import db
from kadi.modules.accounts.models import User
from kadi.modules.oauth2server.models.authorization_code import (
    OAuth2ServerAuthorizationCode,
)
from kadi.modules.oauth2server.models.client import OAuth2ServerClient


class AuthorizationCodeGrant(grants.AuthorizationCodeGrant):
    """The authorization code grant type is used to obtain both access
    tokens and refresh tokens and is optimized for confidential clients.
    Since this is a redirection-based flow, the client must be capable of
    interacting with the resource owner's user-agent (typically a web
    browser) and capable of receiving incoming requests (via redirection)
    from the authorization server."""

    TOKEN_ENDPOINT_AUTH_METHODS = ["client_secret_basic"]

    def save_authorization_code(
        self, code: str, request: Flask.request_class
    ) -> OAuth2ServerAuthorizationCode:
        """Saves the authorization code in the database.

        Args:
            code (str): Authorization Code
            request (Flask.request_class): Latest request

        Returns:
            OAuth2ServerAuthorizationCode: Authorization Code
        """
        code_challenge = request.data.get("code_challenge")
        code_challenge_method = request.data.get("code_challenge_method")

        authorization_code = OAuth2ServerAuthorizationCode.create(
            code=code,
            client=request.client,
            user=request.user,
            redirect_uri=request.redirect_uri,
            code_challenge=code_challenge,
            code_challenge_method=code_challenge_method,
        )

        db.session.commit()

        return authorization_code

    def query_authorization_code(
        self, code: str, client: OAuth2ServerClient
    ) -> OAuth2ServerAuthorizationCode:
        """Querys the authorization code for a specific client to validate it.

        Args:
            code (str): Authorization Code to be queried
            client (OAuth2ServerClient): OAuth Client

        Returns:
            OAuth2ServerAuthorizationCode: Authorization Code
        """
        authorization_code = OAuth2ServerAuthorizationCode.query_by_code_and_client_id(
            code=code, client_id=client.client_id
        )

        if authorization_code and not authorization_code.is_expired():
            return authorization_code

    def delete_authorization_code(self, authorization_code: str):
        """Deletes the authorization code from the database.

        Args:
            authorization_code (str): Authorization Code to be deleted
        """
        db.session.delete(authorization_code)

    def authenticate_user(self, authorization_code: str) -> User:
        """Authenticates the user by the authorization code

        Args:
            authorization_code (str): Authorization code

        Returns:
            User: User to which the authorization code belongs
        """
        return User.query.get(authorization_code.user_id)

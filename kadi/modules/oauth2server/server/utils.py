# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.ext.db import db
from kadi.modules.oauth2server.models.token import OAuth2ServerToken


def save_token(token, request):
    """Saves an OAuth2 token in the database

    Args:
        token (unkown): generated OAuth2 token
        request (unkown): request
    """
    OAuth2ServerToken.create(client=request.client, user=request.user, **token)

    db.session.commit()


def query_token(self, token: str, token_type_hint: str) -> OAuth2ServerToken:
    """Queries a given token in the corresponding models.
    This method is necessary because the default instance method of the revocation endpoint
    can't query token if they are saved in a hashed format.

    The parameter 'self' is necessary to have the same signature as the original method of
    the revocation endpoint.

    Args:
        token (str): token as an unhashed string
        token_type_hint (str): hint which token type to search for - 'access_token' or 'refresh_token'

    Returns:
        OAuth2ServerToken: instance of the token model
    """
    if token_type_hint == "access_token":
        return OAuth2ServerToken.query_by_access_token(token)
    elif token_type_hint == "refresh_token":
        return OAuth2ServerToken.query_by_refresh_token(token)

    item = OAuth2ServerToken.query_by_access_token(token)
    if item:
        return item
    return OAuth2ServerToken.query_by_refresh_token(token)

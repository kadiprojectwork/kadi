# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

from authlib.integrations.flask_oauth2 import AuthorizationServer
from authlib.integrations.sqla_oauth2 import create_query_client_func
from authlib.integrations.sqla_oauth2 import create_revocation_endpoint
from authlib.oauth2.rfc7636 import CodeChallenge
from flask import Flask

from kadi.ext.db import db
from kadi.lib import constants
from kadi.lib.config.core import get_sys_config
from kadi.modules.oauth2server.models.client import OAuth2ServerClient
from kadi.modules.oauth2server.models.token import OAuth2ServerToken
from kadi.modules.oauth2server.server.authorization_code_grant import (
    AuthorizationCodeGrant,
)
from kadi.modules.oauth2server.server.refresh_token_grant import RefreshTokenGrant
from kadi.modules.oauth2server.server.utils import query_token
from kadi.modules.oauth2server.server.utils import save_token


query_client = create_query_client_func(db.session, OAuth2ServerClient)
authorization = AuthorizationServer(query_client=query_client, save_token=save_token)


def config_oauth(app: Flask):
    """Configures the OAuth 2.0 server to be ready to use in the application.

    Args:
        app (Flask): Flask app
    """
    with app.app_context():
        os.environ[
            constants.SYS_CONFIG_OAUTH2_AUTHLIB_INSECURE_TRANSPORT
        ] = get_sys_config(constants.SYS_CONFIG_OAUTH2_AUTHLIB_INSECURE_TRANSPORT)

    authorization.init_app(app)

    authorization.register_grant(AuthorizationCodeGrant, [CodeChallenge(required=True)])
    authorization.register_grant(RefreshTokenGrant)

    revocation_cls = create_revocation_endpoint(db.session, OAuth2ServerToken)
    # There is no other way to overwrite the query token function of the revocation endpoint.
    # An overwrite is needed to be able to search for the tokens. The default method doesn't work
    # because it does not apply the hash.
    revocation_cls.query_token = query_token

    authorization.register_endpoint(revocation_cls)

# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from authlib.oauth2.rfc6749 import grants

import kadi.lib.constants as const
from kadi.ext.db import db
from kadi.lib.config.core import get_sys_config
from kadi.modules.accounts.models import User
from kadi.modules.oauth2server.models.token import OAuth2ServerToken


class RefreshTokenGrant(grants.RefreshTokenGrant):
    """Grant endpoint for refresh_token grant type."""

    TOKEN_ENDPOINT_AUTH_METHODS = ["client_secret_basic"]

    def __init__(self, request, server):
        """Initializes the refresh token grant.

        Args:
            request (unkown): request
            server (unkown): OAuth server
        """
        super().__init__(request, server)

        self.INCLUDE_NEW_REFRESH_TOKEN = (
            get_sys_config(const.SYS_CONFIG_OAUTH2_INCLUDE_NEW_REFRESH_TOKEN) or False
        )

    def authenticate_refresh_token(self, refresh_token: str) -> OAuth2ServerToken:
        """Authenticates the refresh token.

        Args:
            refresh_token (str): Refresh token

        Returns:
            OAuth2ServerToken: Instance of token model
        """
        token = OAuth2ServerToken.query_by_refresh_token(refresh_token)
        if token and token.is_refresh_token_active():
            return token

    def authenticate_user(self, refresh_token: str) -> User:
        """Authenticates the user by the refresh token

        Args:
            refresh_token (str): Authorization code

        Returns:
            User: the user to which the authorization code belongs
        """
        return User.query.get(refresh_token.user_id)

    def revoke_old_credential(self, refresh_token: str):
        """Revokes old credentials

        Args:
            refresh_token (str): Refresh token
        """
        refresh_token.revoked = True

        db.session.add(refresh_token)
        db.session.commit()

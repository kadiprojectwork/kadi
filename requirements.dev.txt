black==22.12.0
build==0.10.0
jsonschema==4.17.3
myst-parser==0.18.1
# Version >=3 requires Python >=3.8.
pre-commit==2.21.0
pylint==2.15.10
pytest==7.2.1
pytest-cov==4.0.0
python-dotenv==0.21.1
rfc3339-validator==0.1.4
# Version >=6 requires Python >=3.8.
Sphinx==5.3.0
sphinx-rtd-theme==1.1.1
Sphinx-Substitution-Extensions==2022.2.16
sphinxcontrib-httpdomain==1.8.1
# Version >=4 requires updates of limits and importlib-metadata first.
tox==3.27.1
twine==4.0.2
watchdog==2.2.1

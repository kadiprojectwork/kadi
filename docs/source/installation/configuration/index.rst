.. _installation-configuration:

Configuration
=============

This section explains the most important configuration items which may need to be set or
changed in order to make |kadi| work properly. The configuration is typically specified
via a corresponding configuration file. The location of this file and which of the
configuration items must be set or changed depends on the installation method. It is
therefore recommended to have a working (or work in progress) |kadi|
:ref:`installation <installation>` before reading through this section.

Note that the configuration file of |kadi| is a normal Python file. This means that it
always has to be syntactically correct, but it also allows imports, calculations and all
other Python features. For example, it can be used in combination with some useful
constants defined in the |kadi| source code (using the ``kadi`` Python package):

.. code-block:: python3

    import kadi.lib.constants as const

    SOME_KEY = const.ONE_GB
    ANOTHER_KEY = 5 * const.ONE_DAY

    # Rest of the config file.

.. note::
    Whenever changing the configuration file in production environments once |kadi| is
    installed successfully, it may be necessary to restart the application and/or
    dependent services for any changes to take effect, similar to performing an
    :ref:`update <installation-production-updating>`.

.. _installation-configuration-sysadmins:

Sysadmins
---------

Some configurations can also be adjusted via a graphical sysadmin interface, which also
does not require a restart of the application. This interface can be found below the
*Settings* menu item in the dropdown menu on the far right of the navigation bar in
|kadi|, once a user is set as a sysadmin. To initially set a user as sysadmin, the Kadi
CLI can be used after installing and configuring |kadi| successfully:

.. code-block:: bash

    sudo su - kadi                # Switch to the kadi user (in production environments)
    kadi users sysadmin <user_id> # Set the user given by its ID via <user_id> as sysadmin

If not already known, the (persistent) ID of a user can be retrieved via the web
interface on the overview page of the corresponding user.

.. note::
    Configuration values set via this interface take precedence over any values
    specified via a configuration file.

Reference
---------

The following sections contain the configuration reference related to
:ref:`authentication <installation-configuration-authentication>` and other
:ref:`settings <installation-configuration-settings>`. Note that all listed default
values correspond to production environments.

.. toctree::
    :maxdepth: 1

    authentication
    settings

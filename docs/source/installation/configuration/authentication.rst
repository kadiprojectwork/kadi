.. _installation-configuration-authentication:

Authentication
==============

This section explains all configuration related to user authentication.

.. data:: AUTH_PROVIDERS

    This configuration value specifies the authentication providers to be used for
    logging in to |kadi|. One or more providers can be specified as a list of
    dictionaries, where each dictionary needs to at least contain the type of the
    authentication provider. Please see the sections below for more details about each
    provider.

    Defaults to:

    .. code-block:: python3

        [
            {
                "type": "local",
            },
        ]

.. tip::
    :ref:`Sysadmins <installation-configuration-sysadmins>` can also manage some aspects
    regarding existing users via the graphical sysadmin interface.

Provider configuration
----------------------

Each authentication provider may contain various, provider-specific configuration
options that may need to be changed in order to make the provider work properly. The
following sections describe each provider and its options in more detail. Note that all
of the listed configuration values represent the defaults of the respective provider.

Local
~~~~~

Accounts based on local authentication are managed by |kadi| itself. Therefore, this
option requires separate accounts to use |kadi|, but is also the easiest option to set
up and use. Local accounts can also change their own password and email address. Email
addresses of local accounts are not confirmed by default.

.. code-block:: python3

    {
        # The type of the authentication provider.
        "type": "local",
        # The title of the authentication provider that will be shown on the login page.
        "title": "Login with credentials",
        # Whether to allow users that can access the website of Kadi4Mat to register
        # their own local accounts by specifying their desired username, display name,
        # email address and password.
        "allow_registration": False,
        # Whether email confirmation through the website of Kadi4Mat is required for
        # local accounts before any of its features can be used.
        "email_confirmation_required": False,
    }

If not allowing users to register their own accounts, local accounts can also be created
manually via the Kadi CLI after installing and configuring |kadi| successfully:

.. code-block:: bash

    sudo su - kadi    # Switch to the kadi user (in production environments)
    kadi users create # Interactively create a new user

.. tip::
    If local authentication is enabled, :ref:`sysadmins
    <installation-configuration-sysadmins>` can also create local users via the
    graphical sysadmin interface.

LDAP
~~~~

Accounts based on LDAP authentication are managed by a directory service implementing
the LDAP protocol. Therefore, this option allows to use existing user accounts. LDAP
accounts cannot change their own email address, but may be able to change their password
through |kadi|. Email addresses of LDAP accounts are confirmed by default.

.. code-block:: python3

    {
        # The type of the authentication provider.
        "type": "ldap",
        # The title of the authentication provider that will be shown on the login page.
        "title": "Login with LDAP",
        # Whether the LDAP server is an Active Directory.
        "active_directory": False,
        # The IP or hostname of the LDAP server.
        "host": "",
        # The port of the LDAP server to connect with. Besides the default port 389,
        # port 636 is generally used together with SSL.
        "port": 389,
        # The encryption method to use. Can be set to "ldaps" to use SSL or "starttls"
        # to use STARTTLS.
        "encryption": None,
        # Whether to validate the server's SSL certificate if an encryption method is
        # set.
        "validate_cert": True,
        # The base DN where users are stored in the LDAP directory, which will be used
        # to perform a simple bind with a user for authentication and to retrieve the
        # additional user attributes after a successful bind. The former works
        # differently depending on whether the server is an Active Directory. For Active
        # Directories, a UserPrincipalName is constructed from the DN's domain
        # components in the form of "<username>@<domain>", where <username> is the
        # specified username when logging in. Otherwise, the full bind DN is constructed
        # as "<username_attr>=<username>,<users_dn>".
        "users_dn": "",
        # The full DN of a user that should be used to perform any LDAP operations after
        # a successful bind. Per default, the bound (i.e. authenticated) user will be
        # used for these operations.
        "bind_user": None,
        # The password of the "bind_user".
        "bind_pw": None,
        # The LDAP attribute name to use for the username, e.g. "uid" or
        # "sAMAccountName". Will also be used for the display name as fallback.
        "username_attr": "uid",
        # The LDAP attribute name to use for the email.
        "email_attr": "mail",
        # The LDAP attribute name to use for the display name. If "firstname_attr" and
        # "lastname_attr" have been specified, these two attributes will take
        # precedence.
        "displayname_attr": "displayName",
        # The LDAP attribute name to use for the first name of the display name instead
        # of "displayname_attr", e.g. "givenName". Must be used together with
        # "lastname_attr".
        "firstname_attr": None,
        # The LDAP attribute name to use for the last name of the display name instead
        # of "displayname_attr", e.g. "sn". Must be used together with "firstname_attr".
        "lastname_attr": None,
        # Whether to allow LDAP users to change their password via Kadi4Mat. This uses
        # the LDAP Password Modify Extended Operation to perform the password change, so
        # keep in mind that any password hashing is done server-side only.
        "allow_password_change": False,
        # Whether to send the old password when changing it, which might be required for
        # some LDAP servers.
        "send_old_password": False,
    }

Shibboleth
~~~~~~~~~~

Accounts based on Shibboleth authentication are managed by one or more institutions.
Therefore, this option allows to use existing user accounts. Shibboleth accounts cannot
change their own email address or password. Email addresses of Shibboleth accounts are
confirmed by default.

Shibboleth has been tested with the official *Shibboleth Service Provider 3* in
combination with the Apache web server using the *mod_shib* module. However, configuring
Shibboleth authentication is currently outside the scope of this documentation, as it
requires additional dependencies, setup and configuration that also depend on the
specific environment and Identity Providers that are to be used for authentication.

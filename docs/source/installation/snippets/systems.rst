.. note::
    The installation has currently been tested under the following Debian-based Linux
    distributions, which the instructions and provided scripts are also based on:

    * **Debian 10** (Buster)
    * **Debian 11** (Bullseye)
    * **Ubuntu 20.04 LTS** (Focal Fossa)
    * **Ubuntu 22.04 LTS** (Jammy Jellyfish)

    It is generally recommended to use the latest version of the respective
    distribution.

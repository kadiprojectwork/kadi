.. _installation-development-docker:

Installation via Docker
=======================

The installation via Docker is preferable when one wants to quickly get up and running
with a working development environment. It is still recommended to check out the
:ref:`manual <installation-development-manual>` installation, as some aspects are
explained there in much more detail. Note that compared to the other installation
methods, Elasticsearch will never be used at the moment.

Installing the dependencies
---------------------------

The only dependency that is needed for this type of installation is `Docker
<https://www.docker.com>`__, specifically the Docker Engine as well as Docker Compose
for running multi-container Docker applications.

The former can be installed running the following commands:

.. code-block:: bash

    sudo apt install apt-transport-https ca-certificates curl gnupg lsb-release
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt update && sudo apt install docker-ce docker-ce-cli containerd.io

The latest Docker Compose binary has to be installed separately, which can be done like
this:

.. code-block:: bash

    sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose

If Docker Compose is not usable inside a terminal via ``docker-compose`` afterwards,
make sure that the ``/usr/local/bin`` directory is included in the ``PATH`` environment
variable.

Running the application
-----------------------

To initially start the application as well as all necessary services, the following
script included in the |kadi| source code can be used, which will first build all
services and then start them in the background:

.. code-block:: bash

    sudo ${HOME}/workspace/kadi/bin/run_docker.sh

There are various ways to manage and interact with the services, i.e. the running Docker
containers, after starting them. The most common commands using ``docker-compose`` are
explained below. For more detailed instructions it is recommended to check out the
official documentations of `Docker <https://docs.docker.com>`__ and `Docker Compose
<https://docs.docker.com/compose>`__. **Note that the** ``docker-compose`` **command
needs access to the** ``docker-compose.yml`` **file in the project's root directory, so
it is recommended to always run it directly inside that directory.**

To attach a terminal to the live log output of all services, including timestamps, the
following command can be used:

.. code-block:: bash

    sudo docker-compose logs -tf

To stop and start all services, the following commands can be used respectively:

.. code-block:: bash

    sudo docker-compose stop
    sudo docker-compose start

To run arbitrary commands inside a container, the following command can be used:

.. code-block:: bash

    sudo docker-compose exec <service> <command>

For example, the following commands, which are run in the context of the ``kadi``
service, are especially useful for development:

.. code-block:: bash

    sudo docker-compose exec kadi kadi db sample-data                                  # Setup some sample users and resources
    sudo docker-compose exec kadi kadi celery worker -B --loglevel=INFO                # Start Celery to run background tasks
    sudo docker-compose exec kadi python -m smtpd -n -c DebuggingServer localhost:8025 # Start a local debugging SMTP server

For more information about these commands, please refer to the :ref:`manual
<installation-development-manual>` installation instructions.

Updating the application
------------------------

After updating the application code via git, it should be enough to restart all services
using:

.. code-block:: bash

    sudo docker-compose restart

In some cases, it may be necessary to rebuild the main ``kadi`` image or to update
existing images of other services, which can both be done by running ``run_docker.sh``
again as described above.

While mainly intended for productive installations, it is also recommended to take a
look at the :ref:`update notes <installation-production-updating-notes>`, since some of
the listed changes may also be relevant for development environments.

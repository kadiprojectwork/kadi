.. _installation-development-python:

.. include:: ../snippets/python.rst

When using the :ref:`virtualenvwrapper <development-general-tools-virtualenvwrapper>`
tool, the new environment can still be managed using the tool after initially creating
it, as long as the new virtual environment was created in the directory specified by the
``WORKON_HOME`` environment variable.

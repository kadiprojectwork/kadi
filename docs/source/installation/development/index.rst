.. _installation-development:

Development
===========

.. include:: ../snippets/systems.rst

This section describes how to install |kadi| in a development environment. There are
currently three ways to perform the installation: :ref:`manual
<installation-development-manual>` installation, installation via :ref:`Docker
<installation-development-docker>` containers and a :ref:`hybrid
<installation-development-hybrid>` installation combining both approaches. Manual
installation, as well as the hybrid approach, include quite a few more manual steps.
However, these approaches also offer much more flexibility in terms of how the
development environment is set up and are therefore the recommended installation methods
at the moment.

Regardless of the method of installation, the first step is always to obtain the source
code of |kadi| using `git <https://git-scm.com>`__, which might have to be installed
first using:

.. code-block:: bash

    sudo apt install git

Before actually obtaining the code, it is recommended to create a fork of the `main
repository <https://gitlab.com/iam-cms/kadi>`__ first. Afterwards, the code can be
cloned into a local directory via SSH (recommended) or HTTPS, the latter being shown in
the following command. Note that the ``<username>`` placeholder needs to be substituted
with the correct username/namespace that the new fork resides in:

.. code-block:: bash

    git clone https://gitlab.com/<username>/kadi.git ${HOME}/workspace/kadi

This will copy the code into the ``workspace/kadi`` directory in the current user's home
directory. This directory can of course be changed freely, however, the rest of this
documentation assumes that the source code resides there and that all commands are run
within this directory:

.. code-block:: bash

    cd ${HOME}/workspace/kadi

To be able to update the code from the central repository, it should be added as an
additional remote, often called ``upstream`` (note that the default remote after
cloning, pointing to the new fork, is always called ``origin``):

.. code-block:: bash

    git remote add upstream https://gitlab.com/iam-cms/kadi.git

.. toctree::
    :maxdepth: 1

    manual
    docker
    hybrid
    python

Testing
=======

All existing tests are currently focused on the backend (i.e. Python) code and can be
found in the ``tests`` directory. For writing and running tests, `pytest
<https://docs.pytest.org/en/stable>`__ is used.

Setting up the environment
--------------------------

For testing code locally, a separate database needs to be created first. The setup is
similar to before, please see how to configure :ref:`PostgreSQL
<installation-development-manual-configuration-postgresql>` for a reminder. **When
prompted for a password, it is recommended to use** ``kadi_test``. This way, the default
testing configuration of the application does not need to be changed.

.. code-block:: bash

    sudo -Hiu postgres createuser -P kadi_test
    sudo -Hiu postgres createdb -O kadi_test kadi_test -E utf-8

Running tests
--------------

Pytest should discover all tests automatically when run inside the project's root
directory:

.. code-block:: bash

    pytest

This will run all backend tests using the current local environment and configuration
values defined in ``pytest.ini``. Pytest also includes tons of command line options,
e.g. to only run specific tests or to print any debugging outputs defined in tests,
which will be suppressed otherwise. Please refer to the official pytest documentation
for details.

#!/usr/bin/env python3
import os
import subprocess
import sys
from collections import OrderedDict

import click


REQUIREMENTS_FILES = [
    ("requirements.txt", "-"),
    ("requirements.dev.txt", "dev"),
    ("requirements.prod.txt", "prod"),
]


def _get_max_len(header, requirements, key=None):
    if len(requirements) == 0:
        return len(header)

    if key is None:
        max_value_len = max(len(package) for package in requirements)
    else:
        max_value_len = max(len(item[key]) for item in requirements.values())

    return max(len(header), max_value_len)


@click.command()
def check_requirements():
    """Check all Python dependencies for updates."""
    requirements = OrderedDict()

    # Parse the requirement files to collect all directly specified dependencies.
    for filename, extra in REQUIREMENTS_FILES:
        filepath = os.path.join(os.path.dirname(__file__), "..", filename)

        with open(filepath, encoding="utf-8") as f:
            for line in f.readlines():
                # Skip comments.
                if line.startswith("# "):
                    continue

                parts = line.split("==")

                package = parts[0]
                # Remove the environment markers, if applicable.
                version = parts[1].split(";")[0].strip()

                requirements[package] = {
                    "extra": extra,
                    "current": version,
                    "latest": None,
                }

    # Get the latest versions of all outdated packages via pip. Suppress stderr in order
    # to ignore possible output about new pip versions.
    result = subprocess.run(
        ["pip", "list", "--outdated"], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL
    )
    lines = [line.strip() for line in result.stdout.decode().splitlines()]

    # Ignore the first two header lines when iterating.
    for line in lines[2:]:
        # Stop after the first empty line, in case there is more output after the
        # package list.
        if not line:
            break

        parts = line.split()
        package = parts[0]

        if package in requirements:
            requirements[package]["latest"] = parts[2]

    # Remove all packages that are already up to date. Make a copy of the keys so we can
    # mutate the requirements while iterating.
    for package in list(requirements):
        requirement_meta = requirements[package]

        if (
            not requirement_meta["latest"]
            or requirement_meta["current"] == requirement_meta["latest"]
        ):
            del requirements[package]

    if len(requirements) == 0:
        click.echo("All requirements are up to date.")
        sys.exit(0)

    # Print all packages and versions.
    package_header = "Package"
    extra_header = "Extra"
    current_ver_header = "Current"
    latest_ver_header = "Latest"

    package_len = _get_max_len(package_header, requirements)
    extra_len = _get_max_len(extra_header, requirements, "extra")
    current_ver_len = _get_max_len(current_ver_header, requirements, "current")
    latest_ver_len = _get_max_len(latest_ver_header, requirements, "latest")

    click.echo(
        f"{package_header.ljust(package_len)}"
        f"  {extra_header.ljust(extra_len)}"
        f"  {current_ver_header.ljust(current_ver_len)}"
        f"  {latest_ver_header.ljust(latest_ver_len)}"
    )
    click.echo(
        f"{'=' * package_len}"
        f"  {'=' * extra_len}"
        f"  {'=' * current_ver_len}"
        f"  {'=' * latest_ver_len}"
    )

    for package, requirement_meta in requirements.items():
        click.echo(
            f"{package.ljust(package_len)}"
            f"  {requirement_meta['extra'].ljust(extra_len)}"
            f"  {requirement_meta['current'].ljust(current_ver_len)}"
            f"  {requirement_meta['latest'].ljust(latest_ver_len)}"
        )


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter
    check_requirements()

#!/usr/bin/env bash
set -e

cd "$(dirname $(readlink -f $0))/.."

docker-compose build --build-arg USER_ID=${SUDO_UID:-$(id -u)} --build-arg GROUP_ID=${SUDO_GID:-$(id -g)}
docker-compose up -d

# Kadi4Mat

**Kadi4Mat** is the **Karlsruhe Data Infrastructure for Materials Science**, an
open source software for managing research data. For more information about the
project, please see its [website](https://kadi.iam-cms.kit.edu) and
[documentation](https://kadi4mat.readthedocs.io/en/stable).

## Installation

While the packaged code of Kadi4Mat can easily be installed as a Python package
via [pip](https://pypi.org/project/kadi), a complete installation requires a
few additional dependencies and considerations. Please refer to the [stable
documentation](https://kadi4mat.readthedocs.io/en/stable) for full installation
instructions.

## Development

Contributions to the code are always welcome. However, please consider creating
an issue first, as described below, if you are planning to make larger changes.
Please refer to the [latest
documentation](https://kadi4mat.readthedocs.io/en/latest) for instructions on
how to set up a development environment of Kadi4Mat as well as other useful
information.

In order to merge any contributions back into the main repository, please open
a corresponding [merge
request](https://gitlab.com/iam-cms/kadi/-/merge_requests). Depending on the
changes, please make sure to add appropriate tests, documentation,
translations, etc. and add a corresponding entry to the changelog in
`HISTORY.md` if necessary. You may also add yourself as a contributor to
`AUTHORS.md`.

## Issues

For any issues regarding Kadi4Mat (bugs, suggestions, discussions, etc.) please
use the [issue tracker](https://gitlab.com/iam-cms/kadi/-/issues) of this
project. Make sure to add one or more fitting labels to each issue in order to
keep them organized. Before creating an issue, please also check whether a
similar issue is already open first. Note that creating new issues requires a
GitLab account.

For **bugs** in particular, please use the provided `Bug` template when
creating an issue, which also adds the `Bug` label to the issue automatically.
For **security-related** issues or concerns, please prefer contacting one of
the current
[maintainers](https://gitlab.com/iam-cms/kadi/-/blob/master/AUTHORS.md)
directly rather than creating a public issue.
